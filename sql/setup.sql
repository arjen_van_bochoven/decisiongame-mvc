DROP TABLE IF EXISTS admin;
CREATE TABLE IF NOT EXISTS admin (
  id INTEGER PRIMARY KEY AUTO_INCREMENT AUTO_INCREMENT,
  username VARCHAR(255) DEFAULT NULL,
  password VARCHAR(255) DEFAULT NULL,
  language VARCHAR(255) DEFAULT NULL
);

DROP TABLE IF EXISTS calculator_usage;
CREATE TABLE IF NOT EXISTS calculator_usage (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id INTEGER DEFAULT NULL,
  username VARCHAR(255) DEFAULT NULL,
  session_id INTEGER DEFAULT NULL,
  gamenumber INTEGER DEFAULT NULL,
  roundnumber INTEGER DEFAULT NULL,
  `usage` INTEGER DEFAULT NULL,
  `datetime` DATETIME DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS chat;
CREATE TABLE IF NOT EXISTS chat (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id INTEGER DEFAULT NULL,
  username VARCHAR(255) DEFAULT NULL,
  partner_id INTEGER DEFAULT NULL,
  partnername VARCHAR(255) DEFAULT NULL,
  chattext TEXT DEFAULT NULL,
  session_id INTEGER DEFAULT NULL,
  gamenumber INTEGER DEFAULT NULL,
  roundnumber INTEGER DEFAULT NULL,
  `datetime` DATETIME DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX history ON chat (user_id, partner_id, session_id, gamenumber);
CREATE INDEX no_history ON chat (user_id, partner_id, session_id, gamenumber, roundnumber);

DROP TABLE IF EXISTS config_games;
CREATE TABLE IF NOT EXISTS config_games (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  session_id INTEGER DEFAULT NULL,
  game INTEGER DEFAULT NULL,
  number_of_rounds INTEGER DEFAULT NULL,
  threshold INTEGER DEFAULT NULL
);

DROP TABLE IF EXISTS game_control;
CREATE TABLE IF NOT EXISTS game_control (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  session_id INTEGER DEFAULT NULL,
  game INTEGER DEFAULT NULL,
  `datetime` DATETIME DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS question_links;
CREATE TABLE IF NOT EXISTS question_links (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT NULL,
  link VARCHAR(255) DEFAULT NULL,
  history INTEGER DEFAULT NULL,
  partnerchange INTEGER DEFAULT NULL
);

DROP TABLE IF EXISTS partnerships;
CREATE TABLE IF NOT EXISTS partnerships (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  session_id INTEGER DEFAULT NULL,
  gamenumber INTEGER DEFAULT NULL,
  roundnumber INTEGER DEFAULT NULL,
  user_id1 INTEGER DEFAULT NULL,
  user_id2 INTEGER DEFAULT NULL,
  username1 VARCHAR(255) DEFAULT NULL,
  username2 VARCHAR(255) DEFAULT NULL
);
CREATE UNIQUE INDEX partnerships_session_game_round_user ON partnerships (session_id, gamenumber, roundnumber, user_id1);

DROP TABLE IF EXISTS program_flow;
CREATE TABLE IF NOT EXISTS program_flow (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT NULL,
  filename VARCHAR(255) DEFAULT NULL,
  type VARCHAR(255) DEFAULT NULL,
  sortorder INTEGER DEFAULT NULL,
  next INTEGER DEFAULT NULL
);

DROP TABLE IF EXISTS game_results;
CREATE TABLE IF NOT EXISTS game_results (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  session_id INTEGER DEFAULT NULL,
  game INTEGER DEFAULT NULL,
  round INTEGER DEFAULT NULL,
  user_id INTEGER DEFAULT NULL,
  username VARCHAR(255) DEFAULT NULL,
  CPA TEXT DEFAULT NULL,
  IPA TEXT DEFAULT NULL,
  threshold_reached INTEGER DEFAULT NULL,
  total_profit_ThisRound INTEGER DEFAULT NULL,
  total_profit_ThisGame INTEGER DEFAULT NULL,
  total_profit_ThisSession INTEGER DEFAULT NULL,
  partner_id INTEGER DEFAULT NULL,
  partnername VARCHAR(255) DEFAULT NULL,
  `datetime` DATETIME DEFAULT CURRENT_TIMESTAMP
);
CREATE UNIQUE INDEX session_game_user ON game_results (session_id, game, user_id);
CREATE UNIQUE INDEX session_user ON game_results (session_id, user_id);
CREATE UNIQUE INDEX sess_game_round_user ON game_results (session_id, game, round, user_id);

DROP TABLE IF EXISTS config_sessions;
CREATE TABLE IF NOT EXISTS config_sessions (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  session_name VARCHAR(255) DEFAULT NULL,
  `datetime` DATETIME DEFAULT CURRENT_TIMESTAMP,
  comment TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS settings;
CREATE TABLE IF NOT EXISTS settings (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  session_id INTEGER DEFAULT NULL,
  name VARCHAR(255) DEFAULT NULL,
  value VARCHAR(255) DEFAULT NULL,
  comment TEXT DEFAULT NULL
);
CREATE INDEX settings_session_id ON settings (session_id);
CREATE UNIQUE INDEX settings_session_id_name ON settings (session_id, name);

DROP TABLE IF EXISTS various_texts;
CREATE TABLE IF NOT EXISTS various_texts (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  type VARCHAR(255) DEFAULT NULL,
  content TEXT DEFAULT NULL,
  sortorder INTEGER DEFAULT NULL,
  next INTEGER DEFAULT NULL,
  language INTEGER DEFAULT NULL,
  history INTEGER DEFAULT NULL,
  partnerchange INTEGER DEFAULT NULL
);

DROP TABLE IF EXISTS game_round_timecheck;
CREATE TABLE IF NOT EXISTS game_round_timecheck (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id INTEGER DEFAULT NULL,
  username VARCHAR(255) DEFAULT NULL,
  session_id INTEGER DEFAULT NULL,
  gamenumber INTEGER DEFAULT NULL,
  roundnumber INTEGER DEFAULT NULL,
  `datetime` DATETIME DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS admin_user_control;
CREATE TABLE IF NOT EXISTS admin_user_control (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id INTEGER DEFAULT NULL,
  username VARCHAR(255) DEFAULT NULL,
  logged_in INTEGER DEFAULT NULL,
  ready_for_game1 INTEGER DEFAULT NULL,
  ready_for_game2 INTEGER DEFAULT NULL,
  ready_for_game3 INTEGER DEFAULT NULL,
  session_id INTEGER DEFAULT NULL
);
CREATE UNIQUE INDEX admin_user_control_session_user ON admin_user_control (user_id, session_id);

DROP TABLE IF EXISTS user_stages;
CREATE TABLE IF NOT EXISTS user_stages (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  session_id INTEGER DEFAULT NULL,
  user_id INTEGER DEFAULT NULL,
  username VARCHAR(255) DEFAULT NULL,
  module INTEGER DEFAULT NULL,
  part INTEGER DEFAULT NULL,
  `datetime` DATETIME DEFAULT CURRENT_TIMESTAMP
);
CREATE UNIQUE INDEX user_stages_session_user ON user_stages (session_id, user_id);
CREATE INDEX user_stages_username ON user_stages (username);

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(255) DEFAULT NULL,
  password VARCHAR(255) DEFAULT NULL,
  session_id INTEGER DEFAULT NULL,
  history INTEGER DEFAULT NULL,
  partnerchange_game1 INTEGER DEFAULT NULL,
  partnerchange_game2 INTEGER DEFAULT NULL,
  partnerchange_game3 INTEGER DEFAULT NULL,
  language VARCHAR(255) DEFAULT NULL
);
CREATE UNIQUE INDEX users_username ON users (username);

