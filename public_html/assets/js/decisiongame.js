//******************************** Decision game ****************************************
	
	// On wait page, check if admin signals to continue
	function LoopAjax_WaitForAdmin()
	{
		$.get(baseurl + "user/xhttp_wait", function(data) {
			
			if(data.indexOf('yes') != -1)
			{
				document.GoNext_form.submit();
			}
			else
			{
				// Only queue new settimeout when GET request is handled
				setTimeout("LoopAjax_WaitForAdmin()", 2000); //todo: get from settings
			}
			
		});
		
	}
	
	
	// Calculator function
	function Result() {
				var x = 0;
				x = eval(window.document.calculator.Display.value);
	  			window.document.calculator.Display.value = x;

	  			// this logs the usage of the calculator in the database, no output will be sent back
	  			//window.document.calculator.submit()
				var url = $('form[name=calculator]').attr( 'action' );
				$.post(url , { use: true });
		}
		
	// Calculator function
	function Add(character) {
	  			window.document.calculator.Display.value =
	  			window.document.calculator.Display.value + character;
		}
		
	
	// Chatbox loop
	function check_chat()
	{
		$.getJSON(chat_callback, {last: last_chat_entry}, function(data)
		{
			if( "content" in data ) {
			    // get new content
				$('#chatbox_content').html(data.content);
				
				//Scroll to bottom
				$('#chatbox_content').animate({ scrollTop: $('#chatbox_content')[0].scrollHeight }, 400);
				var hoch = document.getElementById('chatbox_content').clientHeight;
				document.getElementById('chatbox_content').scrollTop = 10 * hoch;
				
				// Set last entry date
				last_chat_entry = data.last;
			}
			
			// Only queue new settimeout when GET request is handled
			setTimeout("check_chat()", chatbox_frequency);
			
		});
		
	}
	
	// Convert mysql date to js dateObj
	function parseDate(input)
	{
		// Split datetime
		var parts = input.match(/(\d+)/g);
		
		// new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
		return new Date(parts[0], parts[1]-1, parts[2], parts[3], parts[4], parts[5]); // months are 0-based
	}
	
	// Get current server time
	function serverTime() { 
	    var time = null; 
	    $.ajax({url: servertime_callback, 
	        async: false, dataType: 'text', 
	        success: function(text) { 
	            time = new Date(text); 
	        }, error: function(http, message, exc) { 
	            time = new Date(); 
	    }}); 
	    return time; 
	}
	
	// Check if partner is ready
	function partner_ready()
	{
		$.get(game_callback, function(data) {
		  
			if(data.indexOf('yes') != -1)
			{
				// Trigger calculate profits
				$.post( xhttp_calculate_url, { calculate: true }, function( data ) {
					// Jump to next round
					$("#GoToNextRound_form").trigger("submit");
				});
			}
			else
			{
				// Only queue new settimeout when GET request is handled
				setTimeout("partner_ready()",  partner_frequency); 
			}
			
		});
	}
	
	// CHeck if partner for next round is available
	function wait_for_partner()
	{
		$.get(waitforpartner_callback, function(data) {
		  
			if(data.indexOf('yes') != -1)
			{
				// Jump to next round
				$("#GoToNextRound_form").trigger("submit")
			}
			else // For Jef
			{
				// Only queue new settimeout when GET request is handled
				setTimeout("wait_for_partner()",  partner_frequency);
			}
			
		});
		
	}

	
	// Check if a number is an integer
	function isInt(n) {
	   return n % 1 == 0;
	}
	
	// Validator for form
	function validate(form)
	{
		// Run validation
		var resourcesToIPA = $( form ).find('input[name="resourcesToIPA"]').val();
		var resourcesToCPA = $( form ).find('input[name="resourcesToCPA"]').val();
		
		// Check if value is numeric
		if( isNaN(resourcesToIPA) || isNaN(resourcesToCPA))
		{
			alert('Alleen numerieke waarden zijn toegestaan'); //TODO get from var
			return;
		}
		
		// Check for negative value
		if( resourcesToIPA < 0 || resourcesToCPA < 0 )
		{
			alert(error_negative);
			return;
		}

		// Check for float
		if( ! isInt(resourcesToIPA) || ! isInt(resourcesToCPA) )
		{
			alert(error_decimal);
			return;
		}
		
		// Check for total
		if ( (parseInt(resourcesToIPA) + parseInt(resourcesToCPA)) != resources ) 
		{
			alert(error_values);
			return;
		}
		
		return true;
	}
	
	// Callback from countDown timer
	function liftOff()
	{
		document.decision_form.resourcesToCPA.value=0; //todo (jqueryfy)
		document.decision_form.resourcesToIPA.value=0; //todo (jqueryfy)

		submit_game_results(true);
	}
	
	// Submit game results
	function submit_game_results(timeout)
	{
		if(timeout)
		{
			$("#decision_form input[name=timeout]").val('1');
		}
		
		url = $("#decision_form").attr( 'action' );
		
		$.post( url, $("#decision_form").serialize(),
		  function( data ) {
			// Start partner check loop
			partner_ready();
		});		
	}
	
	// Color time red if no minutes left
	function timerColor(periods)
	{
		// Minutes is index 5
		if(periods[5] == 0)
		{
			$("#theTime").css('color','#FF0000');
		}
	}
	
	// Hide invest form
	function hideInvestForm()
	{
		$('#decision_area').hide();
		$('#game_decision').css('border', "solid blue 1px");
		$('#register_message').css('visibility', 'visible');
		$('#time_rests_message').html(remaining_time);
	}
	
// New jquery implementation
$(document).ready(function() {
	
	// Debug dropdown
	$('.debug_session select').change(function(event) {
		$('.debug_session form').trigger("submit")
	});
	
	// Start countdown timer
	if (typeof game_end_time != 'undefined')
	{
		$("#theTime").countdown({ 
		    until: parseDate(game_end_time),
			serverSync: serverTime,
			onExpiry: liftOff,
			alwaysExpire: true,
			format: 'MS',
			onTick: timerColor,
			layout:'{mnn}:{snn}'}
		);
	}
	
	// Check if results are in
	if (typeof results_posted != 'undefined')
	{
		// Start partner ready loop
		partner_ready();
		
		// blank out the invest-button and the fields CPA and IPA
		hideInvestForm();
	}
	
	// Start chatbox loop
	if (typeof chat_callback != 'undefined')
	{
		check_chat();
	}
	
	// Start waitforpartner loop - waits for new partner
	if (typeof waitforpartner_callback != 'undefined')
	{
		wait_for_partner();
	}

	// Submit handler chat form
	$("#chat_form").submit(function(event) {
		
		event.preventDefault();
		
		// Submit form
		$.post( $(this).attr('action'), $(this).serialize(),
		  function( data ) {
			// Delete contents of line
			$('#chatline input[name=chatline]').val('');
		  }
		);
	});
		
	// Decision form submit
	$("#decision_form").submit(function(event) {

		event.preventDefault();
		
		// Validate form
		if( ! validate(this) ) return;
				
		// Submit game results and start partner ready loop
		submit_game_results()
		
		// blank out the invest-button and the fields CPA and IPA
		hideInvestForm();
		
	  });
	
	// Calculator
	$('form[name=calculator]').submit(function(event) {
		
		event.preventDefault();
		
		// Call Result method
		Result();
	});
});

	  
