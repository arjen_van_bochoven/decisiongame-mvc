<?php if ( !defined( 'KISS' ) ) exit;
	
	//===============================================
	// Default settings.
	//===============================================

	// Path to system folder, with trailing slash
	$system_path = '../system/'; 

	// Path to app folder, with trailing slash
	$application_folder = '../app/';

	// Path to view directory, with trailing slash
	$view_path = $application_folder.'views/';

	// Path to controller directory, with trailing slash
	$controller_path = $application_folder.'controllers/';

	// Relative to the webroot, with trailing slash
	$subdirectory = '/';
	
	// Index page (leave blank if .htaccess is used, otherwise use index.php)
	//$index_page = 'index.php';

	// HTTP host, no trailing slash
	$webhost = 'http://'.$_SERVER[ 'HTTP_HOST' ];
	
	// Uri protocol $_SERVER variable that contains the correct request path, 
	// e.g. 'REQUEST_URI', 'QUERY_STRING', 'PATH_INFO', etc.
	// defaults to AUTO
	//$uri_protocol = 'AUTO';

	// Routes
	$routes = array();
	$routes['user(/.*)?']	= "user$1";
	$routes['auth(/.*)?']	= "auth$1";
	$routes['admin(/.*)?']	= "admin$1";
	$routes['.*\.php']= "user/advance/question"; // Default route
	
	// PDO Datasource
	$pdo_dsn = 'mysql:host=localhost;dbname=VU_DecisionGame';
	$pdo_user = 'decisiongame';
	$pdo_pass = 'theekom';

	// Timezone See http://www.php.net/manual/en/timezones.php for valid values
	$timezone = 'Europe/Brussels';
	
	//===============================================
	// Globals
	//===============================================
	$GLOBALS['sitename']='Default Site';
	$GLOBALS['lang_path']= $application_folder.'lang/nl.php';
	$GLOBALS['version'] = '0.7.0';
	
	// Debug level (set from 0 to 4)
	define('DEBUG', 1 );