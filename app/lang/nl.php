<?php if( ! defined('KISS') ) die('No direct access allowed');
/* ------------------------------------------------------------
name:				strings_NL.php
date:				1 december 2011
version:			version 1.0
author:			Rita Standhaft
purpose:			strings in Dutch; 
-------------------------------------------------------------*/        


$lang = array(

	//	-------------------------------------------------------- login-velden
		"str_USERNAME"						=> 	"Gebruikersnaam",
		"str_PASSWORD"						=> 	"Wachtwoord",

		
	// -------------------------------------------------------- buttons		
		"button_OK"							=> 	"OK",
		"button_CANCEL" 					=> 	"Annuleren",
		"button_LOGOUT"					=>		"Afmelden",
		"button_FORWARD"					=>		"Ga verder",
		"button_SAVE"						=>		"Opslaan",
		"button_NEW_SESSION"				=>		"Sessie toevoegen",
		"button_EXPORT_USERLIST"		=>		"Gebruikerslijst opslaan als ...",
		"button_SET_USERCONNECTIONS"	=>		"Leg de partners vast",
		"button_START"						=>		"start",
		"button_START_GAME1"				=>		"start spel 1",
		"button_START_GAME2"				=>		"start spel 2",
		"button_START_GAME3"				=>		"start spel 3",
		"button_END_GAME"					=>		"Beeindig het spel",
		"button_EXCEL_CHAT"				=>		"Opslaan van de chat als Excel-bestand ...",
		"button_EXCEL_CALCULATOR"		=>		"Opslaan van calculator-gebruik als Excel-bestand ...",
		"button_EXCEL_GAMERESULTS"		=>		"Opslaan van game-results als Excel-bestand ...",
		"button_EXCEL_USERSTAGES"		=>		"Opslaan van user-activiteit als Excel-bestand ...",
		"button_CLEAN"						=>		"Opruimen, tabellen opschonen",
		"button_GAME_RESULTS"			=>		"Resultaten opslaan als ...",
		"button_ENTER_DECISION"			=>		"Investeer eenheden",
		"button_SEND_CHAT"				=>		"Verstuur",
		
		
	// -------------------------------------------------------- strings used in administrator-screen
		"str_SESSION_NAME"				=> 	"sessie",
		"str_TODAY"							=>		"Vandaag",
		"str_DATE"							=>		"Datum",

		"str_NUMBER_OF_GAMES"			=>		"aantal spellen",
		"str_GAME"							=>		"Spel",
		"str_NUMBER_OF_ROUNDS"			=>		"aantal rondes per spel",
		"str_THRESHOLD_PER_GAME"      =>    "drempelwaarde per spel",
		"str_NUMBER_OF_USERS"         =>    "spelers",
		"str_SESSION_DATE"            =>    "datum aanmaak",
		"str_ALREADY_PLAYED"          =>    "reeds gespeeld",
		"str_PARTNERCHANGE_SESSION"	=>		"Partner-konfiguratie: in ieder spel",
		"str_PARTNERCHANGE_GAME"		=>		"Partner-konfiguratie in iedere ronde",
		"str_PARTNERCHANGE_NO"			=>		"dezelfde partner",
		"str_PARTNERCHANGE_YES"			=>		"een andere partner",
		"str_CHATHISTORY"					=>		"Gebruik van de chat-historie",
		"str_NO"								=>		"Nee",
		"str_YES"							=>		"Ja",
		"str_SESSION_COMMENT"			=> "Commentaar",
		
		"str_AVAILABLE_USER"				=>		"aantal beschikbare gebruikers",
		"str_LOGGEDIN_USER"				=>		"waarvan ingelogd",
		"str_USER_MONITORING"			=>		"waarvan",
		"str_LOGGED_IN"					=>		"ingelogd",
		"str_READY_FOR_GAME1"			=>		"klaar voor spel 1",
		"str_READY_FOR_GAME2"			=>		"klaar voor spel 2",
		"str_READY_FOR_GAME3"			=>		"klaar voor spel 3",

	// -------------------------------------------------------- strings used in administrator-end-screen
		"str_EE_FACTOR"					=>		"Omrekenfactor Experimentele Euro's in echte Euro's",
		"str_VALUTA"						=>		"&euro;",
		"str_USERID"						=>		"user_id",
	//	"str_USERNAME"						=>		"username",
		"str_PROFIT"						=>		"winst",
		
		
	// -------------------------------------------------------- strings used in user-text_and_question-screens
		"str_QUESTIONS"					=> 	"Vragenlijst",


	// -------------------------------------------------------- strings used in user-game-screen
		"str_GAME"							=> 	"Spel nummer",
		"str_ROUND"							=>		"Ronde nummer",

		"str_CHARACTERISTICS"			=>		"Investerings kenmerken",
		"str_RESOURCES"					=> 	"Aantal beschikbare eenheden per ronde",
		"str_FIXED_BONUS"					=> 	"Vaste gezamenlijke bonus voor succesvolle gezamenlijke productie",
		"str_RESOURCE_ALLOCATION"		=> 	"Waarde van ge&iuml;nvesteerde eenheden",
		"str_RESOURCE_EXPLANATION1"	=>		"per eenheid ge&iuml;nvesteerd in gezamenlijke productie activiteiten boven de vereiste investering",
		"str_RESOURCE_EXPLANATION2"	=>		"per eenheid ge&iuml;nvesteerd in eigen productie",
		"str_RESOURCE_EXPLANATION3"	=>		"per eenheid door partner ge&iuml;nvesteerd in gefaalde gezamenlijke productie",
		"str_RESOURCE_EXPLANATION4"	=>		"De gecre&euml;rde winst door middel van deze investeringen behoort volledig tot je persoonlijke winst.",
		"str_RESOURCE_EXPLANATION5"	=>		"De totale gezamenlijk gecre&euml;rde winst door middel van succesvolle gezamenlijke productie wordt in gelijke delen aan jou en je partner toegewezen.",
		"str_RESOURCE_EXPLANATION6"	=>		"Gedurende een spel is de minimaal vereiste investering voor succesvolle gezamenlijke productie iedere ronde hetzelfde.",
		"str_RESOURCE_EXPLANATION7"	=>		"(EE 250.000 voor jou en EE 250.000 voor je partner)",
		"str_RESOURCE_EXPLANATION8"	=>		"(EE 100 voor jou en EE 100 voor je partner)",
		"str_DECISION"						=>		"Uw beslissing",
		"str_DECISION_TEXT"				=>		"Geef hiernaast aan hoeveel eenheden je wilt investeren in gezamenlijke productie en hoeveel eenheden je wilt investeren in eigen productie. Het totaal dient 150 eenheden te bedragen.",
		"str_RESOURCES_TO_CPA"			=>		"eenheden in gezamenlijke productie",
		"str_RESOURCES_TO_IPA"			=>		"eenheden in eigen productie",
		"str_REMAINING_TIME"				=>		"Resterende tijd om je beslissing te nemen",
		"str_REMAINING_TIME_2"			=>		"Resterende tijd van deze ronde",

		"str_LAST_ROUND"					=>		"Informatie afgelopen ronde",
		"str_LAST_ROUND_TEXT1a"			=>		"Afgelopen ronde had u 150 eenheden ter beschikking, waarvan u ",
		"str_LAST_ROUND_TEXT1b"			=>		" eenheden heeft ge&iuml;nvesteerd in CPA en ",
		"str_LAST_ROUND_TEXT1c"			=>		" eenheden in IPA.",
		"str_LAST_ROUND_TEXT2_yes"		=>		"Aangezien de benodigde drempelwaarde vor succesvolle co&ouml;peratieve productie activiteiten werd ge&iuml;nvesteerd, is de helft van de gecre&euml;rde waarde door middel van succesvolle co&ouml;peratieve productie activiteiten aan uw winst voor deze ronde toegevoegd.",
		"str_LAST_ROUND_TEXT2_no"		=>		"Aangezien de benodigde drempelwaarde vor succesvolle co&ouml;peratieve productie activiteiten niet werd ge&iuml;nvesteerd, is er geen waarde gecre&euml;rd door succesvolle co&ouml;peratieve productie activiteiten.",
		"str_LAST_ROUND_TEXT3a"			=>		"Uw totale winst in deze ronde is ",
		"str_LAST_ROUND_TEXT3b"			=>		" Experimentele Euro's.",

		"str_DECISION_HISTORY"			=>		"Resultaten",
		"str_ROUND_NUMBER"				=>		"Ronde",
		"str_GAME_PARTNER"				=>		"Partner",
		"str_CONTROLLED_RESOUCES"		=>		"Aantal beschikbare eenheden",
		"str_INVESTED_RESOURCES_CPA"	=>		"Eenheden ge&iuml;nvesteerd in gezamenlijke productie",
		"str_INVESTED_RESOURCES_IPA"	=>		"Eenheden ge&iuml;nvesteerd in eigen productie",
		"str_THRESHOLD"					=>		"Minimaal vereiste investering voor succesvolle gezamenlijke productie",
		"str_IPA_IN_EURO"					=>		"Totale winst in Experimentele Euro's door eigen productie",
		"str_ROUND_PROFIT"				=>		"Totale winst in Experimentele Euro's in deze ronde",
		"str_GAME_PROFIT"					=>		"Totale winst in Experimentele Euro's in dit spel",
		"str_THRESHOLD_REACHED_yes"	=>		"behaald",
		"str_THRESHOLD_REACHED_no"		=>		"niet behaald",

		"str_CALCULATOR"					=>		"Calculator",
		"str_CHATBOX"						=>		"Chat box",
		"str_CHAT_PARTNER"				=>		"partner",


		"str_GAMERESULTS"					=> 	"Resultaten van de afgelopen ronde",

		"str_WAITING_INFO"				=> 	"Je wacht nog even totdat iedereen is ingelogd.",
		"str_WAITING_PARTNER_INFO"		=> 	"Je wacht nog even op je volgende partner.",
		"str_WAITING_ADMIN_INFO_1"		=> 	"Je wacht nog even totdat iedereen klaar is om aan het eerste spel te beginnen.",
		"str_WAITING_ADMIN_INFO"		=> 	"Je wacht nog even totdat iedereen klaar is om aan het volgende spel te beginnen.",
		"str_WAIT_FOR_PARTNER"			=>		"Wachten op ronde",


	// -------------------------------------------------------- strings used in screen "end of game"	
		"str_END_OF_GAME"					=>		"Einde spel",
		"str_END_OF_GAME_GO_FORWARD"	=>		"Dit was de laatste ronde van dit spel.<BR><BR>Klik op  \"Ga verder\"  om verder te gaan.",


	// -------------------------------------------------------- messages and error-messages	
		"str_CHECK_CPA_PLUS_IPA"		=>		"Het totaal van je investering (eenheden in eigen productie plus gezamenlijke productie) dient 150 eenheden te zijn. ",
		"str_CHECK_NEGATIVE"				=>		"Negative bedragen zijn niet toegestaan.",
		"str_CHECK_DECIMAL"				=>		"Alleen hele euro-bedragen zijn toegestaan.",
		"str_DECLARATION_REGISTERED"	=>		"Je investering is geregistreerd.<BR><BR>Je wacht nu even totdat je partner ook heeft besloten.",
      "str_NUMBER_OF_USERS_NOT_CORRECT"   => "Aantal ingelogde gebruikers is kleiner dan 16 of is geen even getal -> geen partnerschema mogelijk",
		"str_CANNOT_START"				=>		"Aantal gebruikers, klaar voor de volgende stap, komt niet overeen met het totaal aantal gebruikers.",

	// -------------------------------------------------------- other	


	// -------------------------------------------------------- one last empty string, so all other string-settings can end with comma's	
		"str_LAST"				=>		""
		
		
);
	

?>
