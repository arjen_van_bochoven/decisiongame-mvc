<?php
//! Model Calculator gebruik per speler
class Calc_usage extends Model {

	function __construct($user_id ='', $session_id='', $gamenumber='', $roundnumber='')
	{
		parent::__construct('id','calculator_usage'); //primary key = id; tablename = genes
		$this->rs['id'] = 0;
		$this->rs['user_id'] = 0;
		$this->rs['username'] = '';
		$this->rs['session_id'] = 0;
		$this->rs['gamenumber'] = 0;
		$this->rs['roundnumber'] = 0;
		$this->rs['usage'] = 0;
		$this->rs['datetime'] = date('Y-m-d H:i:s'); 
		
		// Automatically load info for this session
		if ($user_id && $session_id && $gamenumber && $roundnumber)
		{
			$this->retrieve_one('user_id=? AND session_id=? AND gamenumber=? AND roundnumber=?', 
								array($user_id, $session_id, $gamenumber, $roundnumber));
		}
	}
}