<?php
class Partnerships extends Model {
	
	protected $playerlist;
	
	function __construct($user_id = '', $session_id = '', $gamenumber = '', $roundnumber = '')
	{
		parent::__construct('id','partnerships'); //primary key, tablename
		$this->rs['id'] = 0;
		$this->rs['session_id'] = 0;
		$this->rs['gamenumber'] = 0;
		$this->rs['roundnumber'] = 0;
		$this->rs['user_id1'] = 0;
		$this->rs['user_id2'] = 0;
		$this->rs['username1'] = '';
		$this->rs['username2'] = '';
		if ($user_id && $session_id && $gamenumber && $roundnumber)
		{
			$this->retrieve_one('user_id1=? AND session_id=? AND gamenumber=? AND roundnumber=?',
								array($user_id, $session_id, $gamenumber, $roundnumber));
		}
	}
	
	/**
	 * Create partnerships
	 *
	 * @return void
	 * @author Arjen van Bochoven
	 **/
	function create_partnerships($session_id, $users)
	{
		$this->session_id = $session_id;
		
		// Generate name lookup array
		$this->playerlist = array();
		foreach( $users AS $user_obj)
		{
			$this->playerlist[$user_obj->user_id] = $user_obj->username;
		}
		
		// Only player ids
		$players = array_keys($this->playerlist);
		
		// Get game => number of rounds
		$game_config = new Game_config();
		$games = $game_config->retrieve_many('session_id=?', $session_id);
		foreach( $games as $game_obj)
		{
			$game_rounds[$game_obj->game] = $game_obj->number_of_rounds;
		}
				
		// Create proplist (to store history, partnerchange)
		$proplist = array_fill_keys($players, array(0,0,0,0));
			
		// Split history, no history
		list ($history, $no_history) = $this->split_group($players);
	
		// Set history in proplist
		foreach($history as $id) $proplist[$id][0] = 1;
		
		// Main loop
		foreach(array($history, $no_history) AS $arr)
		{
			// Split diff, same history
			list($diff, $same) = $this->split_group($arr);
		
			// Set partnerchange game 1 in proplist
			foreach($diff as $id) $proplist[$id][1] = 1;
			
			$game = 1;
			
			// Get rounds for this game
			$rounds = $game_rounds[$game];
			
			// Match game 1 different
			$this->match_players($diff, 0, $game, $rounds);
			// Match game 1 same
			$this->match_players($same, 1, $game, $rounds);
	
			// Game 2 en 3 same split, different match
			for ($game=2; $game <= 3; $game++) {
			
				// Different partners
				list($diff1, $same1) = $this->split_group($diff);
			
				// Set game n
				foreach($diff1 as $id) $proplist[$id][$game] = 1;
				
				// Get rounds for this game
				$rounds = $game_rounds[$game];
			
				// Match game n different different
				$this->match_players($diff1, 0, $game, $rounds);
				// Match game n different same
				$this->match_players($same1, 1, $game, $rounds);
			
				// Same partners
				list($diff1, $same1) = $this->split_group($same);
			
				// Set game n
				foreach($diff1 as $id) $proplist[$id][$game] = 1;
			
				// Match game n same different
				$this->match_players($diff1, 0, $game, $rounds);
				// Match game n same same
				$this->match_players($same1, 1, $game, $rounds);
			}
		}
		
		// Store proplist in users
		$user = new Users();
		foreach($proplist as $id => $p)
		{
			$user->retrieve($id)->merge(array(
					'history' => $p[0],
					'partnerchange_game1' => $p[1],
					'partnerchange_game2' => $p[2],
					'partnerchange_game3' => $p[3]))->update();
		}
	}
	
	function match_players($basegroup, $same=0, $game=1, $rounds=7)
	{
		$this->gamenumber = $game;
		
		for ($i=1; $i <= $rounds; $i++) 
		{
			// Copy group
			$group = $basegroup;
						
			// Offset
			$offset = $same ? $game - 1 : $i + $game;
			while($group)
			{
				# First player
				list($p1) = array_splice($group, 0, 1);
				
				# Second player
				list($p2) = array_splice($group, $offset % count($group), 1);
				
				// Insert connection for player 1
				$this->id = 0;
				$this->roundnumber = $i;
				$this->user_id1 = $p1;
				$this->user_id2 = $p2;
				$this->username1 = $this->playerlist[$p1];
				$this->username2 = $this->playerlist[$p2];
				$this->create();
				// Insert connection for player 2
				$this->id = 0;
				$this->user_id1 = $p2;
				$this->user_id2 = $p1;
				$this->username1 = $this->playerlist[$p2];
				$this->username2 = $this->playerlist[$p1];
				$this->create();			
			}
		}
	}
	
	function split_group($players)
	{
		$half = count($players) % 4 ? count($players) / 2 + 1 : count($players) / 2;
		return array(array_slice($players, 0, $half), array_slice($players, $half));
	}
		
		
	
	
}