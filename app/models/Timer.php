<?php
class Timer extends Model {

	function __construct($user_id = '', $session_id = '', $gamenumber = '', $roundnumber = '')
	{
		parent::__construct('id','game_round_timecheck'); //primary key, tablename
		$this->rs['id'] = 0;
		$this->rs['user_id'] = 0;
		$this->rs['username'] = '';
		$this->rs['session_id'] = 0;
		$this->rs['gamenumber'] = 0;
		$this->rs['roundnumber'] = 0;
		$this->rs['datetime'] = date('Y-m-d H:i:s'); 
		if ($user_id && $session_id && $gamenumber && $roundnumber)
		{
			$this->retrieve_one('user_id=? AND session_id=? AND gamenumber=? AND roundnumber=?',
								array($user_id, $session_id, $gamenumber, $roundnumber));
		}
		  
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get end time of a game with an offset of minutes and seconds
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function game_end_time($minutes = 0, $seconds = 0)
	{
		$date = new DateTime($this->datetime);
		$date->add(new DateInterval("PT${minutes}M${seconds}S"));
		return $date->format('Y-m-d H:i:s');
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Create with params
	 *
	 * @param multiple
	 * @return object this
	 * @author bochoven
	 **/
	function create($user_id, $username, $session_id, $gamenumber, $roundnumber)
	{
		$this->user_id = $user_id;
		$this->username = $username;
		$this->session_id = $session_id;
		$this->gamenumber = $gamenumber;
		$this->roundnumber = $roundnumber;
		return parent::create();
	}
}