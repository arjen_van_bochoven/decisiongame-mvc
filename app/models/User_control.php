<?php
die('User_control is disabled');
class User_control extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','admin_user_control'); //primary key = id; tablename = question_links
		$this->rs['id'] = $id;
		$this->rs['user_id'] = 0;
		$this->rs['username'] = '';
		$this->rs['logged_in'] = 0;
		$this->rs['ready_for_game1'] = 0;
		$this->rs['ready_for_game2'] = 0;
		$this->rs['ready_for_game3'] = 0;
		$this->rs['session_id'] = 0;
		if ($id)
		  $this->retrieve($id);
	}
		
	// ------------------------------------------------------------------------

	/**
	 * Get number of users logged in and ready for a game in a session
	 *
	 * @param int session
	 * @return array number of players per game
	 * @author bochoven
	 **/
	function user_status($session_id)
	{		
		$sql = "SELECT SUM(logged_in) AS logged_in, 
						SUM(ready_for_game1) AS game1, 
						SUM(ready_for_game2) AS game2, 
						SUM(ready_for_game3) AS game3
		 		FROM admin_user_control 
				WHERE session_id=? AND user_id != 0 GROUP BY session_id";

		$dbh = $this->getdbh();
		$stmt = $dbh->prepare( $sql );
		$stmt->execute(array($session_id));
		if ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			return $rs;
		}
		
		// Return default object
		return (object) array('logged_in' => 0, 'game1' => 0, 'game2' => 0, 'game3' => 0);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get array of users for session
	 *
	 * @param int session_id
	 * @return array with user id's
	 * @author bochoven
	 **/
	function user_array($session_id)
	{
		$dbh = $this->getdbh();
		$sth = $dbh->prepare("SELECT user_id FROM $this->tablename WHERE session_id=?");
		$sth->execute(array($session_id));

		/* Fetch all of the values of the first column */
		return($sth->fetchAll(PDO::FETCH_COLUMN, 0));
	}
	
}