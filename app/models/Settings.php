<?php
class Settings extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','settings'); //primary key = id; tablename = settings
		$this->rs['id'] = $id;
		$this->rs['session_id'] = 0;
		$this->rs['name'] = '';
		$this->rs['value'] = '';
		$this->rs['comment'] = '';
		if ($id)
		  $this->retrieve($id);
	}
}