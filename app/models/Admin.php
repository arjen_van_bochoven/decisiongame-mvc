<?php
//! Model Administrator gebruikersnaam en wachtwoord
class Admin extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','admin'); //primary key, tablename
		$this->rs['id'] = $id;
		$this->rs['username'] = '';
		$this->rs['password'] = '';
		$this->rs['language'] = '';
		if ($id)
		  $this->retrieve($id);
	}
} //END CLASS