<?php
/* ------------------------------------------------------------
name:				admin_set_userconnections_class.php
date:				1 december 2011
version:			version 1.0
author:			Rita Standhaft
purpose:			set the partnership per game/round for every user
-------------------------------------------------------------*/        

// This is not really a model
// Modified: Arjen van Bochoven
// 21 maart 2012
         
class User_connections
{
		   
	protected	$tbl_Users				= "users";
	protected	$tbl_Sessions			= "config_sessions";
	protected	$tbl_Games				= "config_games";
	protected	$tbl_Partnerships		= "partnerships";
	
	protected $session_id = 0;
	protected $user_ids = array(); // Array with user objects
	protected $usernames = array(); // Lookup for usernames
	protected $game_rounds = array(); // Lookup for number of rounds
			
	protected $dbh; // Database handle

	public function __construct ($session_id)
	{
		// Get database handle
		$this->dbh = getdbh();
		
		// Store session_id
		$this->session_id = $session_id;
		
		// get logged-in users
		$user_stgs = new User_stages();
		$this->user_ids = $user_stgs->retrieve_many('session_id=?', $session_id);
		
		// Check if there are users
		if( ! $this->user_ids)
		{
			die( 'Geen spelers ingelogd: kan niet starten');
		}
		// Check if even
		if(count($this->user_ids) % 2)
		{
			die( 'Uneven usercount: kan niet starten'); // fixme: should get to nearest even amount
		}
		
		// Generate name lookup array
		foreach( $this->user_ids AS $user_obj)
		{
			$this->usernames[$user_obj->user_id] = $user_obj->username;
		}
		
		// Get game => number of rounds
		$game_config = new Game_config();
		$games = $game_config->retrieve_many('session_id=?', $session_id);
		foreach( $games as $game_obj)
		{
			$this->game_rounds[$game_obj->game] = $game_obj->number_of_rounds;
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Run Mysql query
	 *
	 * @param string sql
	 * @return object result
	 * @author bochoven
	 **/
	function execSQL($sql)
	{
		$stmt = $this->dbh->prepare( $sql );
		$stmt->execute();
		return $stmt;
	}

	//-----------------------------------
	// SET THE USER_PARTNERSHIPS
	function set_Partnerships ($session_id)
	{

		$group_same_no = array();				// same partner, no history
		$group_diff_no = array();				// different partners, no history
		$group_same_yes = array();				// same partner, with history
		$group_diff_yes = array();				// different partners, with history

		// get logged-in users
		$user_ids = &$this->user_ids;

		// set connections for game 1
		$gamenumber = 1;

		// put each user-id in one of the 4 possible groups
		$half_of_users = count($user_ids) / 2;
		for ( $i=0; $i<$half_of_users; $i++ )
		{
			switch(($i + 4) % 4)
			{
				case 0: // no history, yes partnerchange
					$group_diff_no[]     = $user_ids[$i]->user_id;
					$group_diff_no[]     = $user_ids[$i+$half_of_users]->user_id;
					$history = 0;
					$partnerchange = 1;
					break;
					
				case 1: // yes history, yes partnerchange
					$group_diff_yes[]     = $user_ids[$i]->user_id;
					$group_diff_yes[]     = $user_ids[$i+$half_of_users]->user_id;
					$history = 1;
					$partnerchange = 1;
					break;
					
				case 2: // no history, no partnerchange
					$group_same_no[]     = $user_ids[$i]->user_id;
					$group_same_no[]     = $user_ids[$i+$half_of_users]->user_id;
					$history = 0;
					$partnerchange = 0;
					break;
					
				case 3: // yes history, no partnerchange
					$group_same_yes[]     = $user_ids[$i]->user_id;
					$group_same_yes[]     = $user_ids[$i+$half_of_users]->user_id;
					$history = 1;
					$partnerchange = 0;
					break;
					
			}
			
			
			// Set user 1 props
			$user = new Users($user_ids[$i]->user_id);
			$user->history = $history;
			$user->set("partnerchange_game$gamenumber", $partnerchange);
			$user->update();
			
			// Set user 2 props
			$user = new Users($user_ids[$i + $half_of_users]->user_id);
			$user->history = $history;
			$user->set("partnerchange_game$gamenumber", $partnerchange);
			$user->update();
			
		}
		
		// same partner
		$this->set_Connection_samePartner($group_same_no, $session_id, $gamenumber);
		$this->set_Connection_samePartner($group_same_yes, $session_id, $gamenumber);

		// different partners
		$this->set_Connection_differentPartner($group_diff_no, $session_id, $gamenumber);
		$this->set_Connection_differentPartner($group_diff_yes, $session_id, $gamenumber);


		// each group is divided in two smaller groups
		$group_same_diff_no			= array();				// same partner, different partners, no history
		$group_same_same_no			= array();				// same partner, same partner, no history
		$group_diff_diff_no			= array();				// different partners, different partners, no history
		$group_diff_same_no			= array();				// different partners, same partner, no history
		$group_same_diff_yes			= array();				// same partner, different partners, with history
		$group_same_same_yes			= array();				// same partner, same partner, with history
		$group_diff_diff_yes			= array();				// different partners, different partners, with history
		$group_diff_same_yes			= array();				// different partners, same partner, with history

		// in ieder groep bevindt zich een even aantal; bij het splitsen opletten, dat ook even aantallen worden doorgegeven
		// dus -> uit 8 wordt 4 + 4 en uit 6 wordt 4 + 2
		if ( count($group_same_no) % 4 == 0 )	{ $separate = count($group_same_no)/2; }
		else		{ $separate = count($group_same_no)/2 + 1 ; }
		$group_same_same_no			= array_splice($group_same_no, $separate);
		$group_same_diff_no			= $group_same_no;

		if ( count($group_diff_no) % 4 == 0 )	{ $separate = count($group_diff_no)/2; }
		else		{ $separate = count($group_diff_no)/2 + 1 ; }
		$group_diff_same_no			= array_splice($group_diff_no, $separate);
		$group_diff_diff_no			= $group_diff_no;

		if ( count($group_same_yes) % 4 == 0 )	{ $separate = count($group_same_yes)/2; }
		else		{ $separate = count($group_same_yes)/2 + 1 ; }
		$group_same_same_yes			= array_splice($group_same_yes, $separate);
		$group_same_diff_yes			= $group_same_yes;

		if ( count($group_diff_yes) % 4 == 0 )	{ $separate = count($group_diff_yes)/2; }
		else		{ $separate = count($group_diff_yes)/2 + 1 ; }
		$group_diff_same_yes			= array_splice($group_diff_yes, $separate);
		$group_diff_diff_yes			= $group_diff_yes;

		// set connections for the other games
		$gamenumber = 2;

		// same partner
		$this->set_Connection_samePartner($group_same_same_no, $session_id, $gamenumber);
		$this->set_Connection_samePartner($group_diff_same_no, $session_id, $gamenumber);
		$this->set_Connection_samePartner($group_same_same_yes, $session_id, $gamenumber);
		$this->set_Connection_samePartner($group_diff_same_yes, $session_id, $gamenumber);

		// different partners
		$this->set_Connection_differentPartner($group_same_diff_no, $session_id, $gamenumber);
		$this->set_Connection_differentPartner($group_diff_diff_no, $session_id, $gamenumber);
		$this->set_Connection_differentPartner($group_same_diff_yes, $session_id, $gamenumber);
		$this->set_Connection_differentPartner($group_diff_diff_yes, $session_id, $gamenumber);
	}


	public function set_Connection_samePartner($group, $session_id, $gamenumber)
	{
		$partnerchange = 0;

		if ( $gamenumber == 1 )
		{
			$k = 0;

			// get number of rounds for this game
			$number_of_rounds = $this->game_rounds[$gamenumber];

			// loop over rounds; same partnership in every round
			for ( $r = 1; $r <= $number_of_rounds; $r++ )
			{
				$this->set_combination($group, $session_id, $gamenumber, $r, $k);
			}		
		}
		else 
		{
			$k = 1;

			// get number of games
			$number_of_games	= count($this->game_rounds);

			// loop over games
			for ( $g = 2; $g <= $number_of_games; $g++ )
			{
				// get number of rounds for this game
				$number_of_rounds = $this->game_rounds[$g];

				// loop over rounds; same partnership in every round
				for ( $r = 1; $r <= $number_of_rounds; $r++ )
				{
					$this->set_combination($group, $session_id, $g, $r, $k);
				}		
				// set the counter for the next combination
				$k = $k + 1;
				
				// set partnerchange-property
				foreach ( $group as $user_id )
				{
					$user = new Users($user_id);
					$user->set("partnerchange_game$g", $partnerchange);
					$user->update();
				}
			}		
		}
	}

	public function set_Connection_differentPartner($group, $session_id, $gamenumber)
	{
		$partnerchange = 1;

		if ( $gamenumber == 1 )
		{
			$k = 0;

			// get number of rounds for this game
			$number_of_rounds = $this->game_rounds[$gamenumber];

			// loop over rounds; different partnership in every round
			for ( $r = 1; $r <= $number_of_rounds; $r++ )
			{
				$this->set_combination($group, $session_id, $gamenumber, $r, $k);

				// set the counter for the next combination
				$k = $k + 1;
			}
		}
		else 
		{
			$k = 1;

			// get number of games
			$number_of_games	= count($this->game_rounds);

			// loop over games
			for ( $g = 2; $g <= $number_of_games; $g++ )
			{
				// get number of rounds for this game
				$number_of_rounds = $this->game_rounds[$g];

				// loop over rounds; same partnership in every round
				for ( $r = 1; $r <= $number_of_rounds; $r++ )
				{
					$this->set_combination($group, $session_id, $g, $r, $k);

					// set the counter for the next combination
					$k = $k + 1;
				}
				
				// set partnerchange-property
				foreach ( $group as $user_id )
				{
					$user = new Users($user_id);
					$user->set("partnerchange_game$g", $partnerchange);
					$user->update();
				}
			}		
		}
	}

	public function set_combination($group, $session_id, $gamenumber, $roundnumber, $k)
	{
		// loop over half the users in this group
		for ($u = 0; $u < count($group)/2; $u++ )
		{
			$user_id1         = $group[$u];

			$v                = $u + count($group)/2 + $k;
			if ( $v >= count($group) ) { $v = ($v % (count($group)/2)) + (count($group)/2); }

			$user_id2         = $group[$v];

			$username1        = $this->usernames[$user_id1];
			$username2        = $this->usernames[$user_id2];
						
			$this->insert_connection_into_table ($gamenumber, $roundnumber, $user_id1, $user_id2, $username1, $username2);
			$this->insert_connection_into_table ($gamenumber, $roundnumber, $user_id2, $user_id1, $username2, $username1);
		}
	}

	public function insert_connection_into_table ($gamenumber, $roundnumber, $user_id1, $user_id2, $username1, $username2)
	{
		$sql = "INSERT INTO `" .$this->tbl_Partnerships. "` 
		(session_id, gamenumber, roundnumber, user_id1, user_id2, username1, username2) 
		VALUES (" .$this->session_id. ", " .$gamenumber. ", " .$roundnumber. ", " .$user_id1. ", " .$user_id2. ", '" .$username1. "', '" .$username2. "');";
		$result = $this->execSQL ($sql);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Delete connections from session.
	 *
	 * @param int session_id
	 * @return void
	 * @author bochoven
	 **/
	public function delete_connections($session_id = 0)
	{
		$sql = "DELETE FROM %s WHERE session_id = %d";
		$result = $this->execSQL (sprintf($sql, $this->tbl_Partnerships, $session_id));
	}
}
		
?>