<?php
class Links extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','question_links'); //primary key = id; tablename = question_links
		$this->rs['id'] = $id;
		$this->rs['name'] = '';
		$this->rs['link'] = '';
		$this->rs['history'] = 0;
		$this->rs['partnerchange'] = 0;
		if ($id)
		  $this->retrieve($id);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Override: Retrieve a link based on name and session vars
	 *
	 * @param string where what
	 * @return object
	 * @author bochoven
	 **/
	function retrieve_one($name, $vars)
	{
		$where = 'name=? AND (history=? OR history=2) AND (partnerchange=? OR partnerchange=2)';
		
		$gamenumber = lastchar($name); // Last char of name
		$history = isset($vars['history']) ? $vars['history'] : 0;
		$partnerchange = isset($vars['partnerchange_game'.$gamenumber]) ? $vars['partnerchange_game'.$gamenumber] : 0;
		
		$bindings = array($name, $history, $partnerchange);

		return parent::retrieve_one($where, $bindings);
	}
}