<?php
class Texts extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','various_texts'); //primary key = id; tablename = genes
		$this->rs['id'] = $id;
		$this->rs['type'] = '';
		$this->rs['content'] = '';
		$this->rs['sortorder'] = 0;
		$this->rs['next'] = 0;
		$this->rs['language'] = '';
		$this->rs['history'] = 0;
		$this->rs['partnerchange'] = 0;
		if ($id)
		  $this->retrieve($id);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Override: Retrieve a text based on name and session vars
	 *
	 * @param string where what
	 * @return object
	 * @author bochoven
	 **/
	function retrieve_one($name, $vars)
	{
		$where = 'type=? AND (history=? OR history=2) AND (partnerchange=? OR partnerchange=2) AND sortorder=? ORDER BY sortorder';
		
		$gamenumber = lastchar($name); // Last char of name
		$history = isset($vars['history']) ? $vars['history'] : 0;
		$partnerchange = isset($vars['partnerchange_game'.$gamenumber]) ? $vars['partnerchange_game'.$gamenumber] : 0;
		$page = isset($vars['part'])? $vars['part'] : 1;
		
		$bindings = array($name, $history, $partnerchange, $page);
		
		return parent::retrieve_one($where, $bindings);
	}
	
}