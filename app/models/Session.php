<?php
class Session extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','config_sessions'); //primary key, tablename
		$this->rs['id'] = $id;
		$this->rs['session_name'] = '';
		$this->rs['datetime'] = date('Y-m-d H:i:s'); 
		$this->rs['comment'] = '';
		if ($id)
		  $this->retrieve($id);
	}
}