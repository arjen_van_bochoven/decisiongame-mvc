<?php
class Users extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','users'); //primary key = id; tablename = genes
		$this->rs['id'] = $id;
		$this->rs['username'] = '';
		$this->rs['password'] = '';
		$this->rs['session_id'] = 0;
		$this->rs['history'] = 0;
		$this->rs['partnerchange_game1'] = 0;
		$this->rs['partnerchange_game2'] = 0;
		$this->rs['partnerchange_game3'] = 0;
		$this->rs['language'] = '';
		if ($id)
		  $this->retrieve($id);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get users per section
	 *
	 * @param void
	 * @return array
	 * @author bochoven
	 **/
	function retrieve_users_per_session()
	{
		$sql = 'SELECT session_id, MIN(username) AS min_user, MAX(username) AS max_user FROM users group by session_id';
		
		$dbh = $this->getdbh();
		$stmt = $dbh->prepare( $sql );
		$stmt->execute();
		$return = array();
		while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			$return[$rs->session_id] = $rs;
		}
		
		return $return;
	}
		
	// ------------------------------------------------------------------------

	/**
	 * Get total users per session
	 *
	 * @param int session id
	 * @return int total users
	 * @author bochoven
	 **/
	function total_users_per_session($session_id)
	{
		$sql = 'SELECT COUNT(session_id) AS total FROM users WHERE session_id=? group by session_id';
		
		$dbh = $this->getdbh();
		$stmt = $dbh->prepare( $sql );
		$stmt->execute(array($session_id));
		if ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			return $rs->total;
		}
	}	
}