<?php
class User_stages extends Model {

	// ------------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @param int optional session_id
	 * @param int optional user_id
	 * @return void
	 * @author bochoven
	 **/
	function __construct($session_id = 0, $user_id = 0)
	{
		parent::__construct('id','user_stages'); //primary key; tablename
		$this->rs['id'] = 0;
		$this->rs['session_id'] = 0;
		$this->rs['user_id'] = 0;
		$this->rs['username'] = '';
		$this->rs['module'] = 0;
		$this->rs['part'] = 0;
		$this->rs['datetime'] = date('Y-m-d H:i:s'); 
		if($session_id && $user_id)
		{
			$this->retrieve_one('session_id=? AND user_id=?', array($session_id, $user_id));
		}
	}
}
