<?php
class Results extends Model {

	function __construct($session_id = '', $game = '', $round = '', $user_id = '')
	{
		parent::__construct('id','game_results'); //primary key, tablename
		$this->rs['id'] = 0;
		$this->rs['session_id'] = 0;
		$this->rs['game'] = 0;
		$this->rs['round'] = 0;
		$this->rs['user_id'] = 0;
		$this->rs['username'] = '';
		$this->rs['CPA'] = '';
		$this->rs['IPA'] = '';
		$this->rs['threshold_reached'] = 0;
		$this->rs['total_profit_ThisRound'] = 0;
		$this->rs['total_profit_ThisGame'] = 0;
		$this->rs['total_profit_ThisSession'] = 0;
		$this->rs['partner_id'] = 0;
		$this->rs['partnername'] = '';
		$this->rs['datetime'] = date('Y-m-d H:i:s');
		
		if ($session_id && $game && $round && $user_id)
		{
			$this->retrieve_one('session_id=? AND game=? AND round=? AND user_id=?', 
								array($session_id, $game, $round, $user_id));
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Calc profit this round
	 *
	 * @param object partner result object
	 * @param object settings object
	 * @param object game config object
	 * @return this
	 * @author bochoven
	 **/
	function calc_profit_round($partner, $settings, $game_config)
	{
		// Threshold reached?
		$this->threshold_reached = $this->CPA + $partner->CPA >= $game_config->threshold;
		
		if($this->threshold_reached)
		{
			$T_dependent_value = ( $this->CPA + $partner->CPA - $game_config->threshold ) * 
			 						$settings->resource_allocation_1 * 0.5;
		}
		else
		{
			$T_dependent_value = $partner->CPA * $settings->resource_allocation_3;
		}
		
		$this->total_profit_ThisRound = ($this->IPA * $settings->resource_allocation_2) +
										( $this->threshold_reached * $settings->fixed_bonus * 0.5) +
										$T_dependent_value;
		
		// Get total Profits this game
		$dbh = $this->getdbh();
		$sql = 'SELECT SUM(total_profit_ThisRound) AS game_profits FROM '.$this->enquote( $this->tablename );
		$sql .= ' WHERE session_id=? AND game=? AND user_id=? GROUP BY user_id';
		$stmt = $dbh->prepare( $sql );
		$stmt->execute( array($this->session_id, $this->game, $this->user_id) );
		$rs = $stmt->fetch( PDO::FETCH_ASSOC );
		
		$this->total_profit_ThisGame = $rs['game_profits'] + $this->total_profit_ThisRound;
		
		// Total profits this session
		$sql = 'SELECT SUM(total_profit_ThisRound) AS sess_profits FROM '.$this->enquote( $this->tablename );
		$sql .= ' WHERE session_id=? AND user_id=? GROUP BY user_id';
		$stmt = $dbh->prepare( $sql );
		$stmt->execute( array($this->session_id, $this->user_id) );
		$rs = $stmt->fetch( PDO::FETCH_ASSOC );
		
		$this->total_profit_ThisSession = $rs['sess_profits'] + $this->total_profit_ThisRound;
		
		return $this;
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get profit per user
	 *
	 * @param int session_id
	 * @param int user_id
	 * @return int profit
	 * @author bochoven
	 **/
	function profit_per_user($session_id, $user_id)
	{
		$dbh = $this->getdbh();
		$sql = "SELECT max(total_profit_ThisSession) AS profit_ThisSession 
    			FROM $this->tablename 
    			WHERE session_id = ? AND user_id = ?
    			GROUP BY user_id";
		$stmt = $dbh->prepare($sql);
    	$stmt->execute(array($session_id, $user_id));
		if($row = $stmt->fetch( PDO::FETCH_OBJ ))
		{
			return $row->profit_ThisSession;
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get total profits for each user for this session
	 *
	 * @param int session_id
	 * @return array with objects
	 * @author bochoven
	 **/
	function get_profits_session($session_id)
	{
		$dbh = $this->getdbh();
		$sql = "SELECT user_id, username, max(total_profit_ThisSession) AS profit_ThisSession 
    			FROM $this->tablename 
    			WHERE session_id = ?
    			GROUP BY user_id";
		$stmt = $dbh->prepare($sql);
    	$stmt->execute(array($session_id));
		return $stmt->fetchAll( PDO::FETCH_OBJ );
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get unique sessions
	 *
	 * @param void
	 * @return array
	 * @author bochoven
	 **/
	function retrieve_sessions_played()
	{
		$dbh = $this->getdbh();
		$sql = "SELECT session_id FROM $this->tablename GROUP BY session_id";
		$stmt = $dbh->prepare( $sql );
		$stmt->execute();
		$return = array();
		while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			$return[] = $rs->session_id;
		}

		return $return;
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get history
	 *
	 * @param void
	 * @return array with objects
	 * @author bochoven
	 **/
	function get_history($last_roundnumber)
	{
		$dbh = $this->getdbh();
		
		$settings = game_settings($_SESSION['session_id']);
		$EE = $settings->resource_allocation_2;
		
		$game = lastchar($_SESSION['section']);
		$partnerchange = $_SESSION['partnerchange_game'.$game];
		
		$sql = "SELECT round, CPA, IPA, threshold_reached, (IPA * $EE) AS total_profit_IPAinEuro, 
						total_profit_ThisRound, total_profit_ThisGame 
				FROM $this->tablename 
				WHERE session_id = ? AND game = ? AND user_id = ?";
			
		$bindings = array($_SESSION['session_id'], $game, $_SESSION['id']);
		
		// If no history, only return last round
		if ( ! $_SESSION['history'])
		{
			$sql .= ' AND round = ? LIMIT 1';
		}
		else
		{
			$sql .= 'AND round <= ? ORDER BY round DESC';
		}
		
		$bindings[] = $last_roundnumber;
		
		$stmt = $dbh->prepare( $sql );
		$stmt->execute($bindings);
		$return = array();
		while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			$rs->partnername = $partnerchange ? "partner_$rs->round" : 'partner_1';
			$rs->resources = $settings->resources;
			$return[] = $rs;
		}

		return $return;
		
	}
}