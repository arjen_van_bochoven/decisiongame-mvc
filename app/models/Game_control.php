<?php
class Game_control extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','game_control'); //primary key, tablename
		$this->rs['id'] = $id;
		$this->rs['session_id'] = 0;
		$this->rs['game'] = 0;
		$this->rs['datetime'] = date('Y-m-d H:i:s'); 
		if ($id)
		  $this->retrieve($id);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get games started
	 *
	 * @param int session id
	 * @return array games started for this session
	 * @author bochoven
	 **/
	function games_started($session_id)
	{
		$return = array();
		$games = $this->retrieve_many('session_id=?', $session_id);
		foreach($games as $game)
		{
			$return[] = $game->game;
		}
		
		return $return;
	}
}