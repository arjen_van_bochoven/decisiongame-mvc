<?php
class Programflow extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','program_flow'); //primary key = id; tablename = genes
		$this->rs['id'] = $id;
		$this->rs['name'] = '';
		$this->rs['filename'] = '';
		$this->rs['type'] = '';
		$this->rs['sortorder'] = 0;
		$this->rs['next'] = 0;
		if ($id)
		  $this->retrieve($id);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Retrieve complete flow
	 *
	 * @param void
	 * @return void
	 * @author abn290
	 **/
	function retrieve_many($wherewhat = '', $bindings = '') 
	{
		$dbh = $this->getdbh();
		$sql = 'SELECT * FROM '.$this->tablename;
		$sql .= ' ORDER BY id';
		$stmt = $dbh->prepare( $sql );
		$stmt->execute();
		$arr=array();
		$class=get_class( $this );
		while ( $rs = $stmt->fetch( PDO::FETCH_ASSOC ) ) 
		{
			$myclass = new $class();
			foreach ( $rs as $key => $val )
				if ( isset( $myclass->rs[$key] ) )
					$myclass->rs[$key] = is_scalar( $myclass->rs[$key] ) ? $val : unserialize( $this->COMPRESS_ARRAY ? gzinflate( $val ) : $val );
			$arr[]=$myclass;
		}
		return $arr;
	}
}