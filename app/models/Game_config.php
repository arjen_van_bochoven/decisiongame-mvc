<?php
class Game_config extends Model {

	function __construct($session_id = '', $game_number = '')
	{
		parent::__construct('id','config_games'); //primary key = id; tablename = genes
		$this->rs['id'] = 0;
		$this->rs['session_id'] = 0;
		$this->rs['game'] = 0;
		$this->rs['number_of_rounds'] = 0;
		$this->rs['threshold'] = 0;
		
		// Automatically load info for this session
		if ($session_id && $game_number)
		{
			$this->retrieve_one('session_id=? AND game=?', array($session_id, $game_number));
		}
	}
}