<?php
//! Model Bevat de chat per speler per ronde
class Chat extends Model {

	function __construct($id = '')
	{
		parent::__construct('id','chat'); //primary key = id; tablename = genes
		$this->rs['id'] = $id;
		$this->rs['user_id'] = 0;
		$this->rs['username'] = '';
		$this->rs['partner_id'] = 0;
		$this->rs['partnername'] = '';
		$this->rs['chattext'] = '';
		$this->rs['session_id'] = 0;
		$this->rs['gamenumber'] = 0;
		$this->rs['roundnumber'] = 0;
		$this->rs['datetime'] = date('Y-m-d H:i:s'); 
		if ($id)
		  $this->retrieve($id);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get chatbox content
	 * this is a very expensive function, don't waste any cycles here
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function get_chat()
	{
		$game = lastchar($_SESSION['section']);
		$user_id = $_SESSION['id'];
		
		$dbh = $this->getdbh();
		$condition = $_SESSION['history'] ? '' : "AND roundnumber = " . $_SESSION['part'];
		$sql = 'SELECT chattext, user_id, roundnumber FROM '.$this->enquote( $this->tablename );
		$sql .= " WHERE (user_id = ?  OR user_id = ? ) 
                    AND session_id = ? 
                    AND gamenumber = ? 
					$condition 
        			ORDER BY datetime ASC";
		$stmt = $dbh->prepare( $sql );
		$stmt->execute( array($user_id, $_SESSION['partner'], $_SESSION['session_id'], $game) );
		$chatbox_content = "";
		$partner_classes = array("ptn1", "ptn2", "ptn3");
		$class_mod = count($partner_classes);
		$last_roundnumber = 0;
		
		while ( $row = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			$chatbox_content .= $last_roundnumber == $row->roundnumber ? '' : "</p><hr><p>";

			if ( $row->user_id == $user_id )
			{
				$class = 'me';
				$str_partner = '';
			}
			else
			{
				if ($_SESSION['partnerchange_game1'] OR $_SESSION['partnerchange_game2'] OR $_SESSION['partnerchange_game3'])
				{ 
					$str_partner	= "(partner_$row->roundnumber):  "; 
					$class = $partner_classes[$row->roundnumber % $class_mod];
				}
				else
				{ 
					$str_partner	= "(partner_1):  "; 
					$class = $partner_classes[0];
				}
			}
			
			$chatbox_content .= sprintf('<span class="%s"> r%s | %s</span><br>', $class, $row->roundnumber, $str_partner .$row->chattext);

			$last_roundnumber = $row->roundnumber;
		}
		
		return "<p>$chatbox_content</p>";

	}
}