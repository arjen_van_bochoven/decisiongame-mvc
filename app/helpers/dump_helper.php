<?php

// ------------------------------------------------------------------------

/**
 * Run Mysql query
 *
 * @param string sql
 * @return object result
 * @author bochoven
 **/
function execSQL($sql, $bindings = '')
{
	static $dbh = '';
	
	if ( ! $dbh)
	{
		// Get database handle
		$dbh = getdbh();
	}
	
	if ( is_scalar( $bindings ) )
		$bindings = $bindings ? array( $bindings ) : array();
	
	$stmt = $dbh->prepare( $sql );
	$stmt->execute($bindings);
	return $stmt;
}

// ------------------------------------------------------------------------

/**
 * Dump table info to csv file
 *
 * @param void
 * @return void
 * @author bochoven
 **/
function dump_to_excel($session_id, $tbl_Name)
{
	//Sanitize $tbl_name
	$tbl_Name = preg_replace("/[^A-Za-z0-9_]/i", "", $tbl_Name);
	
	// Filename to dump
	$filename		= $tbl_Name."_".date("Y-m-d",time()) . "_session_" .$session_id;

	switch ($tbl_Name)
	{
		case "calculator_usage": 
			$columns = "username, gamenumber, roundnumber, usage, datetime";
			$sortorder = " ORDER BY user_id, datetime ASC";
			break;
		
		case "chat":
			$columns = "gamenumber, roundnumber, username, chattext, partnername, datetime";
			$sortorder = " ORDER BY session_id, gamenumber, roundnumber ASC";
			break;
		
		case "user_stages":
			$columns = "username, module, datetime";
			$sortorder = " ORDER BY username, datetime ASC";
			break;

		case "game_results":
			$columns = "game, round, username, CPA, IPA, threshold_reached, total_profit_ThisRound, total_profit_ThisGame, total_profit_ThisSession, partnername";
			$sortorder = " ORDER BY session_id, game, round, datetime ASC";
			break;
			
		case "profits_to_pay":
		
			// Get lang strings
			require($GLOBALS['lang_path']);
			// Get config
			$settings = game_settings($session_id);
			
			$columns = "username, max(total_profit_ThisSession), FORMAT((max(total_profit_ThisSession) * $settings->EE_factor), 2)";
			$colnames = "Username, ". $lang['str_PROFIT'] . "_in_ExperimentalEuro, ". $lang['str_PROFIT'] . "_in_RealEuro";
			$sortorder = " GROUP BY username ORDER BY username";
			
			// Rename table
			$tbl_Name = 'game_results';
			break;
			
		case 'users':
			$columns = "username AS a, password, username AS b, username AS c, CONCAT(username, '@vu.nl') AS d";
			$colnames = "username, password, firstname, lastname, email";
			$sortorder = ' ORDER BY username';
			break;
			
		case 'settings':
			$columns = "name, value, comment";
			$sortorder = ' OR session_id = 0 ORDER BY session_id, id';
			break;
		case 'partnerships':
			$columns = "gamenumber, roundnumber, username1, username2";
			$sortorder =  " AND user_id1 < user_id2 ORDER BY gamenumber, roundnumber, user_id1";
			break;
		default:
			echo "table not found: $tbl_Name";
			exit;
	}
	
	$sql = "SELECT $columns FROM $tbl_Name WHERE session_id = ? $sortorder";
	$stmt = execSQL ($sql, $session_id);
	
	// Convert columns to header
	$colnames = isset($colnames) ? $colnames : $columns;
	$field_array = explode(', ', $colnames);
	$excel_data = '"'.implode('";"', $field_array). "\"\n";

	while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) )
	{
			$excel_data .= '"'.implode('";"', $row). '"';
			$excel_data .= "\n";
	} 

	header("Content-type: application/vnd.ms-excel; charset=iso-8859-1");
	header("Content-disposition: csv" . date("Y-m-d") . ".csv");
	header("Content-disposition: filename=".$filename.".csv");
	print $excel_data;
	exit; 
}
