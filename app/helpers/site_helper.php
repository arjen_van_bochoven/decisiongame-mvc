<?php
function url($url='', $fullurl = FALSE)
{
  $s = $fullurl ? WEB_HOST : '';
  $s .= WEB_FOLDER.($url && INDEX_PAGE ? INDEX_PAGE.'/' : INDEX_PAGE).$url;
  return $s;
}

function redirect($uri = '', $method = 'location', $http_response_code = 302)
{
	if ( ! preg_match('#^https?://#i', $uri))
	{
		$uri = url($uri);
	}
	
	if(headers_sent($file, $line))
	{
		printf('<p><a href="%s">%s: You are redirected to %s</a></p>', $uri, 'HEADERS SENT', $uri);
		exit;
	}
	
	switch($method)
	{
		case 'refresh'	: header("Refresh:0;url=".$uri);
			break;
		default			: header("Location: ".$uri, TRUE, $http_response_code);
			break;
	}
	exit;
}


// ------------------------------------------------------------------------

/**
 * Retrieve all game settings (todo: should be a model actually)
 *
 * @param int session_id
 * @return array with settings
 * @author bochoven
 **/
function game_settings($session_id = 0)
{
	static $gamesettings = array();
	
	if( ! isset($gamesettings['session'.$session_id]))
	{
		$gamesettings['session'.$session_id] = array();
		
		$dbh = getdbh();
		
		$sql = "SELECT name, value FROM settings WHERE session_id IN(0, $session_id) ORDER BY session_id, id";
		foreach ($dbh->query($sql) as $row)
		{
			$gamesettings['session'.$session_id][$row['name']] = $row['value'];
		}
		
	}
		
	return (object) $gamesettings['session'.$session_id];
}

// ------------------------------------------------------------------------

/**
 * Debug msg handler
 *
 * @param string message
 * @return void
 * @author bochoven
 **/
function debug($message, $level = 1)
{
	if (defined('DEBUG')) {
		if( $level <= DEBUG)
		{
			echo "<p>$message</p>";
		}
	}
}

// ------------------------------------------------------------------------

/**
 * Get last char of string
 *
 * @param string string
 * @return string char
 * @author bochoven
 **/
function lastchar($str)
{
	if($str != '')
	{
		return $str[strlen($str)-1];
	}
}

// ------------------------------------------------------------------------

/**
 * Replace links and placeholders in db strings
 *
 * @param string text
 * @return string text
 * @author bochoven
 **/
function filter_text($text)
{
	$replace = array(
		"<IMG SRC='include/" => "<IMG SRC='".WEB_FOLDER."assets/gfx/",
		"<<s>>" => "(s)",
	);
	
	// If last text, insert total profit
	if(strpos($text, '<<total_profit_in_euro>>'))
	{
		$result = new Results();
		$profit = $result->profit_per_user($_SESSION['session_id'], $_SESSION['id']);
		
		$settings = game_settings();
		
		$replace['<<total_profit_in_euro>>'] = number_format($profit * $settings->EE_factor, 2);
	}
	
	return str_replace(array_keys($replace), $replace, $text);
}