<?php
//! Controller Authenticatie functies
class auth extends Controller
{	
	function __construct()
	{
		//check_db();
		
	} 
	
	
	//===============================================================
	
	function index()
	{
		redirect('auth/login');
	}
	
	// ------------------------------------------------------------------------

	/**
	 * User login
	 *
	 * @param optional string
	 * @return void
	 * @author bochoven
	 **/
	function login()
	{
		$args = func_get_args();
		$return = implode('/', $args);
		
		$username = isset($_POST['username']) ? $_POST['username'] : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		
		
		$data = array('username' => $username, 'url' => url("auth/login/$return"));
		
		// No password necessary in debug mode
		if(defined('DEBUG') && $username && ! $password)
		{
			$where = 'username=?';
			$bindings = array($username);
			$password = 'dummy';
		}
		else
		{
			$where = 'username=? AND password=?';
			$bindings = array($username, $password);
		}
		
		if ($username && $password)
		{
			$user = new Users();
			if( $user->retrieve_one($where, $bindings))
			{
				// Put all user vars in session
				foreach($user->rs as $k => $v)
				{
					$_SESSION[$k] = $v;
				}
				
				// Set default part to 1
				$_SESSION['part'] = 1;
				
				// Check if a previous session exists
				$stage = new User_stages($_SESSION['session_id'], $_SESSION['id']);
				if($stage->id)
				{
					$_SESSION['flow'] = max(2, $stage->module);
					$_SESSION['part'] = $stage->part;
				}

				redirect($return);
			}
		}

		$obj = new View();
		$obj->view('auth/login', $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Admin login
	 *
	 * @param optional string
	 * @return void
	 * @author bochoven
	 **/
	function login_admin($return='')
	{
		$username = isset($_POST['username']) ? $_POST['username'] : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		
		
		$data = array('username' => $username, 'url' => url("auth/login_admin/$return"));
		
		
		if ($username && $password)
		{
			$user = new Admin();
			if( $user->retrieve_one('username=? AND password=?', array($username, $password)))
			{
				// Destroy all session vars
				session_unset();
				
				$_SESSION['admin'] = 'admin';
				
				redirect($return);
			}
		}

		$obj = new View();
		$obj->view('auth/login', $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Logout
	 *
	 * @param optional string
	 * @return void
	 * @author bochoven
	 **/
	function logout($return = '')
	{
		session_destroy();
		redirect($return);
	}	
}