<?php
//! Controller Administrator functies
class admin extends Controller
{
	function __construct()
	{
		
		if( ! isset($_SESSION['admin']))
		{
			redirect('auth/login_admin/admin');
		}
	}
	
	
	//===============================================================
	
	function index()
	{
		redirect('admin/overview');
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Sessie configuratie
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function overview()
	{
		$data = array(
			'admin' => 'admin', 
			'date_today' => date("j M Y"),
			'sessions' => []);
		
		// Get users per session
		$users = new Users();
		$ups = $users->retrieve_users_per_session();
		
		$results = new Results();
		$res = $results->retrieve_sessions_played();
		
		$sql = "SELECT session_id, session_name, datetime, comment, count(game) as number_of_games, 
				GROUP_CONCAT( number_of_rounds ORDER BY game DESC SEPARATOR ', ') AS rounds, 
				GROUP_CONCAT( threshold ORDER BY game DESC SEPARATOR ', ') AS thresholds
				FROM `config_sessions` cs
				LEFT JOIN `config_games` cg ON(`cs`.`id` = `cg`.`session_id`) 
				GROUP BY `session_name`, session_id, datetime, comment
				ORDER BY session_id DESC";
		
		$dbh = getdbh();
		$stmt = $dbh->prepare( $sql );
		$stmt->execute();
		while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			// Add users per session to result obj
			$sid = $rs->session_id;
			$rs->users = isset($ups[$sid]) ? $ups[$sid]->min_user.' t/m '.$ups[$sid]->max_user : '?';
			
			// Add 'played' to result obj
			$rs->played = in_array($sid, $res) ? 'Ja' : '';
			
			$data['sessions'][] = $rs;
		}

		$obj = new View();
		$obj->view("admin/overview", $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Edit session
	 *
	 * @param int session id, if not given, a new session is created
	 * @return void
	 * @author abn290
	 **/
	function session_edit($session_id = 0)
	{
	    $data['session'] = new Session($session_id);
	    $data['settings'] = game_settings($session_id);
	    $obj = new View();
		$obj->view("admin/session_edit", $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Create new session
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function session_save($session_id = 0)
	{		
		// Sanitize session_name
		$session_name = trim($_POST['session_name']);
		
		// Create new session
		$session = new Session($session_id);
		
		if( ! $session->id)
		{
			$session->create(); // Automatically generates a new id
		}
		
		$session->session_name = $session_name ? $session_name : 'session_'.$session->id;
		$session->comment = trim(htmlentities($_POST['comment']));
		$session->update();
		
		// Update game settings
		$setobj = new Settings();
		$settings = game_settings($session_id);
		foreach($_POST as $k => $v)
		{
			if(isset($settings->$k) && $settings->$k != $v)
			{
				// Update or create setting
				$indb = $setobj->retrieve_one('session_id = ? AND name = ?', array($session->id, $k));
				$setobj->name = $k;
				$setobj->value = $v;
				if($indb)
				{
					$setobj->update();
				}
				else
				{
					$setobj->session_id = $session->id;
					$setobj->id =0;
					$setobj->create();
				}
				// Store new value in settings object
				$settings->$k = $v;
			}
		}
				
		// Create new games
		$game = new Game_config;
		$game->delete_session($session->id);		
		for ( $g = 1; $g <= $settings->number_of_games; $g++ )
		{
			$game->id = '';
			$game->session_id = $session->id;
			$game->game = $g;
			$game->number_of_rounds = $settings->default_number_of_rounds;
			$default_threshold = "default_threshold$g";
			$game->threshold = isset($settings->$default_threshold) ? $settings->$default_threshold : 200;
			$game->create();
		}
		
		
		// Insert users if usercount changes
		$user = new Users();
		if($user->total_users_per_session($session->id) != $settings->number_of_players_per_session)
		{
			$user->delete_session($session->id);
			$user->session_id = $session->id;
			$user->language = $settings->language;
			$chars = 'ABCDFGHJKMNPQRSTUVWXYZ2345689'; // Password chars todo: config
			$pw_length = 4; // Password length todo: config
			for ( $u = 1; $u <= $settings->number_of_players_per_session; $u++ )
			{
				$user->id = '';
				$user->username = sprintf('id%s%03d', $session->id, $u);

				// Generate passwd
				$user->password = '';
				for ($i = 0; $i < $pw_length; $i++) { 
					$user->password .= $chars[(rand() % strlen($chars))];
				}
				$user->create();
			}
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Game control page
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function session($session_id = '')
	{
		if( ! $session_id)
		{
			redirect('admin');
		}
		
		$data = array('session_id' => $session_id, 'admin' => 'admin', 'date_today' => date("j M Y"));
		
		// Get current session
		$session = new Session($session_id);
		$data['session_name'] = $session->session_name;
		
		// Get total available users
		$users = new Users();
		$data['available_users'] = $users->total_users_per_session($session_id);
		
		// Get status of users
		//$user_ctl = new User_control();
		$data['ready_users'] = (object) array('logged_in' => 200, 'game1' => 0, 'game2' => 0, 'game3' => 0);
		
		// Check if there are partnerships for this session
		$partnership = new Partnerships();
		$partnership->retrieve_one('session_id=?', $session_id);
		$data['partnerships_ready'] = $partnership->id ? 1 : 0;
		
		// Check game status
		$game_ctl = new Game_control();
		$data['games_started'] = $game_ctl->games_started($session_id);
		
		// Get some global settings
		$settings = game_settings($session_id);
		$data['admin_frequency'] = $settings->admin_frequency;
		
		$obj = new View();
		$obj->view("admin/session_control", $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Delete all session information todo: use foreign keys
	 *
	 * @param int session id
	 * @return void
	 * @author abn290
	 **/
	function session_delete($session_id)
	{
		$data = array('session_id' => $session_id, 'removed' => 0);
		if(isset($_POST['confirm']))
		{
			$dbh = getdbh();

			// Delete from config_sessions
			$sql = "DELETE FROM config_sessions WHERE id = ?";
			$stmt = $dbh->prepare( $sql );
			$stmt->execute(array($session_id));

			// Delete from other tables
			$tables = array('calculator_usage', 'chat', 'config_games', 'game_control', 'game_results', 'game_round_timecheck', 'partnerships', 'user_stages', 'users');
			foreach ($tables as $table)
			{
				$sql = "DELETE FROM $table WHERE session_id = ?";
				$stmt = $dbh->prepare( $sql );
				$stmt->execute(array($session_id));
			}
			$data['removed'] = 1;
		}
		
		$obj = new View();
		$obj->view("admin/session_delete", $data);
		
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Reset selected session
	 * Deletes all session info from the tables
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function session_reset($session_id)
	{
		$data = array('session_id' => $session_id, 'reset' => 0);
		if(isset($_POST['confirm']))
		{
			$dbh = getdbh();

			// Delete from other tables
			$tables = array('game_round_timecheck', 
							'calculator_usage', 'chat', 'game_control', 
							'game_results', 'partnerships', 'user_stages');
			foreach ($tables as $table)
			{
				$sql = "DELETE FROM $table WHERE session_id = ?";
				$stmt = $dbh->prepare( $sql );
				$stmt->execute(array($session_id));
			}
			$data['reset'] = 1;
		}
		
		$obj = new View();
		$obj->view("admin/session_reset", $data);
		
	}
	
	
	// ------------------------------------------------------------------------

	/**
	 * View/change session variables
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function session_conf($session_id = 0)
	{
		if ( ! $session_id) redirect('admin');
		
		// todo: Check if session is in progress, prevent edit
		
		$data = array('session_id' => $session_id, 'admin' => 'admin', 'date_today' => date("j M Y"));
		
		// Get session info
		$session = new Session($session_id);
		$data['session_name'] = $session->session_name;
		$data['session_obj'] = $session;
		
		// Get total available users
		$users = new Users();
		$data['available_users'] = $users->total_users_per_session($session_id);
		
		// Get settings
		$data['settings'] = game_settings($session_id);
		
		$obj = new View();
		$obj->view("admin/session_conf", $data);
		
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Show partner list
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function partners($session_id='')
	{
		if ( ! $session_id ) redirect('admin');
		
		$data = array('session_id' => $session_id, 'admin' => 'admin', 'date_today' => date("j M Y"));
		
		// Get current session
		$session = new Session($session_id);
		$data['session_name'] = $session->session_name;

		$obj = new View();
		$obj->view("admin/partners", $data);
		
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Show partnerships for user
	 *
	 * @param void
	 * @return void
	 * @author abn290
	 **/
	function user_partners($user_id = 0)
	{
		if (! $user_id ) die('Speler niet gevonden');
		
		$data = array('user_id' => $user_id);
		$obj = new View();
		$obj->view("admin/user_partners", $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Login all users for a session
	 *
	 * @param int session_id
	 * @return void
	 * @author bochoven
	 **/
	function debug_login_users($session_id = '')
	{
		if ( ! $session_id ) die( 'No session given');
		
		$users = new Users();
		$all = $users->retrieve_many('session_id=?', $session_id);
		
		$user_stgs = new User_stages();
		$user_stgs->session_id = $session_id;
		$user_stgs->module = 2;
		$user_stgs->part = 0;
		
		foreach($all as $user)
		{
			// reset id
			$user_stgs->id = 0;
			$user_stgs->user_id = $user->id;
			$user_stgs->username = $user->username;
			$user_stgs->datetime = date('Y-m-d H:i:s'); 
			$user_stgs->create();
		}
		
		redirect('admin/session_progress/'. $session_id);
	}	
		
	
	// ------------------------------------------------------------------------

	/**
	 * Insert game start flag
	 *
	 * @param int session_id
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_start_game($session_id = 0)
	{
		// Sanitize input
		if( ! $session_id) die ('No valid session');
		if( ! isset($_POST['module'])) die ('No module requested');
		
		// Check if there are partnerships for this session
		$partnership = new Partnerships();
		$partnership->retrieve_one('session_id=?', $session_id);
		if( ! $partnership->id) // No partnerships found, create them first
		{
			// get logged-in users
			$user_stgs = new User_stages();
			$users = $user_stgs->retrieve_many('session_id=?', $session_id);
		
			// Check if there are users
			if( ! $users)
			{
				die( 'Geen spelers ingelogd: kan niet starten');
			}
			// Check if even
			if(count($users) % 2)
			{
				die( 'Uneven usercount: kan niet starten'); // fixme: should get to nearest even amount
			}
			
			$partnership->create_partnerships($session_id, $users);
			
			echo 'Partners connecties aangemaakt. ';
		}
		
		
		// Game control
		$game_ctl = new Game_control();
		$game_ctl->retrieve_one('session_id=? AND game=?', array($session_id, $_POST['module']));
		
		if($game_ctl->session_id) die('Dit spel is al gestart');

		$game_ctl->session_id = $session_id;
		$game_ctl->game = $_POST['module'];
		$game_ctl->create();
		
		echo 'Spel gestart';
	}
		
	// ------------------------------------------------------------------------

	/**
	 * Set user connections
	 *
	 * @param string session id
	 * @return void
	 * @author bochoven
	 **/
	function set_userconnections($session_id = 0)
	{
		if ($session_id)
		{
			$user_conn = new User_connections($session_id);
			
			// Delete connections
			$user_conn->delete_connections($session_id);
			
			// Set new connections
			$user_conn->set_Partnerships($session_id);
		}
		
		redirect('admin/session_progress/'.$session_id);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get userlist for this session
	 *
	 * @param int session_id
	 * @return void
	 * @author abn290
	 **/
	function userlist($session_id)
	{
		if ($session_id)
		{
			$obj = new View();
			$obj->view("admin/userlist", array('session_id' => $session_id));
		}		
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get results for this session
	 *
	 * @param int session_id
	 * @return void
	 * @author abn290
	 **/
	function results($session_id)
	{
		if ($session_id)
		{
			$obj = new View();
			$obj->view("admin/session_results", array('session_id' => $session_id));
		}		
	}
	
	
	// ------------------------------------------------------------------------

	/**
	 * Session progress
	 *
	 * @param int sessie id
	 * @return void
	 * @author abn290
	 **/
	function session_progress($session_id = 0)
	{
		if (! $session_id) redirect('admin');
		
		$data = array('session_name' => "Sessie $session_id", 'session_id' => $session_id, 'admin' => 'admin', 'date_today' => date("j M Y"));
		
		
		$obj = new View();
		$obj->view("admin/session_progress", $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * User progress
	 *
	 * @param int user id
	 * @return void
	 * @author abn290
	 **/
	function user_progress($username = 0)
	{
		if (! $username ) die('Speler niet gevonden');
		
		// Admin moves user to another module
		if(isset($_POST['flow']))
		{
			$users = new Users();
			if( ! $user = $users->retrieve_one('username = ?', $username)) die('Speler niet gevonden');
			$user_stage = new User_stages($user->session_id, $user->id);
			$user_stage->module = $_POST['flow'];
			$user_stage->part = 1;
			$user_stage->update();
			redirect("admin/user_progress/$username");
		}
		
		$data = array('admin' => 'admin', 'date_today' => date("j M Y"), 'username' => $username);
		
		
		$obj = new View();
		$obj->view("admin/user_progress", $data);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get stage of each user
	 *
	 * @param int session id
	 * @return json string
	 * @author abn290
	 **/
	function xhttp_user_stages($session_id)
	{
		$dbh = getdbh();
		$stmt = $dbh->prepare( "SELECT user_id, module FROM user_stages WHERE session_id = ? ORDER BY user_id" );
		$stmt->execute(array($session_id));
		while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) ) 
		{
			$outarr[] = sprintf('"%s":%d', $rs->user_id, $rs->module);
		}
		printf('{%s}', implode(',', $outarr));
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get userlist of available users in session in json format
	 *
	 * @param int session id
	 * @return void
	 * @author abn290
	 **/
	function xhttp_user_list($session_id)
	{
		$dbh = getdbh();
		$stmt = $dbh->prepare( 
		"SELECT id, username
			FROM users u
			WHERE session_id = ?" );
		$stmt->execute(array($session_id));
		while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) ) 
		{
			$outarr[] = sprintf('"%s":"%s"', $rs->id, $rs->username);
		}
		printf('{%s}', implode(',', $outarr));
	}
			
	// ------------------------------------------------------------------------

	/**
	 * Dump table to csv
	 *
	 * @param string table
	 * @param int session_id
	 * @return void
	 * @author bochoven
	 **/
	function excel_dump($table='', $session_id=0)
	{
		if (isset($_POST['session_id']) && isset($_POST['table']))
		{
			// Load dump helper
			require(APP_PATH.'helpers/dump_helper'.EXT);
			
			// Dump file
			dump_to_excel($_POST['session_id'], $_POST['table']);
		}
		
		if( $table && $session_id)
		{
			// Load dump helper
			require(APP_PATH.'helpers/dump_helper'.EXT);
			
			// Dump file
			dump_to_excel($session_id, $table);
			
		}
		
		redirect('admin');
	}              
	
}