<?php
//! Controller Speler functies
class user extends Controller
{
	// ------------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function __construct()
	{		
		if( ! isset($_SESSION['username']))
		{
			redirect('auth/login/user');
		}
		
		// No admins
		if( isset($_SESSION['admin'] ))
		{
			redirect('admin');
		}
		
		// Get program flow for user
		if ( ! isset($_SESSION['flow']))
		{
			// Initialize flow (skip login)
			$_SESSION['flow'] = 2;
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Default controller
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function index()
	{
		redirect('user/advance');
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Main user controller, switches to current item in program flow
	 * based on $_SESSION['flow']
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function next()
	{
		// Get current item
		$pf = new Programflow($_SESSION['flow']);
		
		// Store name in session
		$_SESSION['section'] = $pf->name;
		
		$data = array('username' => $_SESSION['username'], 'flow_next' => $pf->next);
		
		// Get game settings
		$settings = game_settings($_SESSION['session_id']);
		
		// Update SESSION with userdata (only necessary after partner connctions are set)
		$user = new Users($_SESSION['id']);
		foreach(array('history', 'partnerchange_game1', 'partnerchange_game2', 'partnerchange_game3') as $att)
		{
			$_SESSION[$att] = $user->{$att};
		}
		
		// Switch based on Program Flow type
		switch($pf->type)
		{
			case 'text':
				$text = new Texts();
				$text->retrieve_one(str_replace('text_', '', $pf->name), $_SESSION);
				$data['text'] = filter_text($text->content);
				$data['part_next'] = $text->next;
				
				// If multiple pages, reset flow to current
				if($data['part_next'])
				{
					$data['flow_next'] = $_SESSION['flow'];
				}
				break;
				
			case 'questions':
				// Get text
				$text = new Texts();
				$text->retrieve_one($pf->name, $_SESSION);
				$data['text'] = filter_text($text->content);
				$data['part_next'] = $text->next;
				
				// If multiple pages, reset flow to current
				if($data['part_next'])
				{
					$data['flow_next'] = $_SESSION['flow'];
				}
				
				// get link to limesurvey
				$links = new Links();
				$links->retrieve_one($pf->name, $_SESSION);
				$data['link'] = $links->link;
				break;
				
			case 'wait':
				$data['roundnumber'] = isset($_SESSION['part']) ? $_SESSION['part'] : '';
				$data['flow'] = $_SESSION['flow'];
								
				// Signal that we are ready for next game
				$game = lastchar($pf->name);
				
				break;
				
			case 'game':
			
				// Get partner
				$partner = new Partnerships($_SESSION['id'], $_SESSION['session_id'], lastchar($pf->name), $_SESSION['part']);
				$_SESSION['partner'] = $partner->user_id2;
				$_SESSION['partnername'] = $partner->username2;
				
				$data['roundnumber'] = $_SESSION['part']; 
				$data['gamenumber'] = lastchar($pf->name); //get last char from name
				
				// Check if we've already posted results for this round
				$result = new Results($_SESSION['session_id'], $data['gamenumber'], $data['roundnumber'], $_SESSION['id']);
				$data['results_posted'] = $result->id ? TRUE : FALSE;
				
				// Get history
				$data['history_list'] = $result->get_history($_SESSION['part'] - 1);
				
				// Check if partner is ready
				if( ! $this->xhttp_wait_for_partner( $xhttp = FALSE))
				{
					// Partner is not available, load wait for partner page
					$data['part_next'] = $_SESSION['part'];
					$obj = new View();
					$obj->view("user/waitforpartner", $data);
					return;
				}
				
				// Get time from table
				$timer = new Timer($_SESSION['id'], $_SESSION['session_id'], lastchar($pf->name), $_SESSION['part']);
				if( ! $timer->id)
				{
					// No timer found, get partner timer
					$timer = new Timer($_SESSION['partner'], $_SESSION['session_id'], lastchar($pf->name), $_SESSION['part']);
					
					// Create timer
					$timer->id = '';
					$timer->create($_SESSION['id'], $_SESSION['username'], $_SESSION['session_id'], lastchar($pf->name), $_SESSION['part']);
				}
				$data['game_end_time'] = $timer->game_end_time($settings->clock_minutes, $settings->clock_seconds);
				
		
				// Get number of rounds
				$game = new Game_config($_SESSION['session_id'], $data['gamenumber']);
				if($_SESSION['part'] < $game->number_of_rounds)
				{
					$data['part_next'] = $_SESSION['part'] + 1;
					$data['flow_next'] = $_SESSION['flow'];
				}
				else
				{
					$data['part_next'] = 1;
				}
				break;
				
			case 'endofgame':
				// Get number of rounds
				$data['gamenumber'] = lastchar($pf->name); //get last char from name
				$game = new Game_config($_SESSION['session_id'], $data['gamenumber']);
				
				$data['part_next'] = 1;
				
				// Get history
				$result = new Results();
				$data['history_list'] = $result->get_history($game->number_of_rounds);
				break;
				
			case 'logout':
				redirect('auth/logout');
				break;
		}
		
		
		$obj = new View();
		$obj->view("user/$pf->type", $data);
		
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Advance game
	 * We only advance the game when there's a valid POST or GET callback from limesurvey
	 *
	 * @param string optional string, used for survey callback
	 * @return void
	 * @author bochoven
	 **/
	function advance($what = '')
	{
		if ($_POST)
		{
			if(isset($_POST['flow']))
			{
				$_SESSION['flow'] = $_POST['flow'];
				$_SESSION['part'] = $_POST['part'] ? $_POST['part'] : 1;
			}
		}
		elseif($what) // Callback from limesurvey
		{
			// Check if we're in a questions section
			$pf = new Programflow($_SESSION['flow']);
			if($pf->type == 'questions')
			{
				$_SESSION['flow'] = $pf->next;
				$_SESSION['part'] = 1;
			}
		}
		
		// Store session information in db
		$stage = new User_stages($_SESSION['session_id'], $_SESSION['id']);
		$stage->module = $_SESSION['flow'];
		$stage->part = $_SESSION['part'];
		if($stage->id)
		{
			$stage->update();
		}
		else
		{
			$stage->session_id = $_SESSION['session_id'];
			$stage->user_id = $_SESSION['id'];
			$stage->username = $_SESSION['username'];
			$stage->create();
		}
		
		redirect('user/next/'.$_SESSION['flow'].'/'.$_SESSION['part']);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Log calculator usage
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_calc_usage()
	{
		if($_POST)
		{
			$game = lastchar($_SESSION['section']);
			
			$calc = new Calc_usage($_SESSION['id'], $_SESSION['session_id'], $game, $_SESSION['part']);

			if ( ! $calc->id )
			{
				$calc->user_id = $_SESSION['id'];
				$calc->username = $_SESSION['username'];
				$calc->session_id = $_SESSION['session_id'];
				$calc->gamenumber = $game;
				$calc->roundnumber = $_SESSION['part'];
				$calc->usage = 1;
				$calc->datetime = date('Y-m-d H:i:s'); 
				$calc->create();
			}
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * XHTTP wait function
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_wait()
	{
		$game_ctl = new Game_control();
		$active_games = $game_ctl->games_started($_SESSION['session_id']);

		echo in_array($_SESSION['flow'], $active_games) ? 'yes' : 'no';
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Chatbox xhttp callback
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_chat()
	{
		// Json return array
		$json = array();

		// Get last date
		$last = isset($_GET['last']) ? $_GET['last'] : 0;
		
		$sql = '(user_id = ?  OR user_id = ? ) 
		        AND session_id = ? 
		        AND gamenumber = ?
				AND datetime > ?';
				
		$bindings = array(	$_SESSION['id'], 
							$_SESSION['partner'],
							$_SESSION['session_id'],
							lastchar($_SESSION['section']),
							$last);
		
		// Check if user has history
		if ( ! $_SESSION['history'])
		{
			$sql .= ' AND roundnumber = ?';
			$bindings[] = $_SESSION['part'];
		}
		
		// Check if there's a newer item
		$chat = new Chat();
		if( $chat->retrieve_one( $sql, $bindings))
		{
			$json['last'] = $chat->datetime;
			$json['content'] = $chat->get_chat();
		}
		echo json_encode($json);
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Chat line receiver
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_send_chat()
	{
		if(isset($_POST['chatline']))
		{
			$chat = new Chat();
			$chat->user_id = $_SESSION['id'];
			$chat->username = $_SESSION['username'];
			$chat->partner_id = $_SESSION['partner'];
			$chat->partnername = $_SESSION['partnername'];
			$chat->chattext = htmlspecialchars($_POST['chatline'], ENT_NOQUOTES);
			$chat->session_id = $_SESSION['session_id'];
			$chat->gamenumber = lastchar($_SESSION['section']);
			$chat->roundnumber = $_SESSION['part'];
			$chat->create();
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Callback to see if partner is ready
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_game()
	{
		if( isset($_SESSION['partner']))
		{
			// Check if partner has posted results
			$result = new Results();
			$result->retrieve_one('user_id=? AND session_id=? AND game=? AND round=?',
						array($_SESSION['partner'], $_SESSION['session_id'], lastchar($_SESSION['section']), $_SESSION['part']));
			if($result->id)
			{
				echo 'yes';
			}
		}
		
		echo 'no';
		
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Check if the partner for the new round is available
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_wait_for_partner($xhttp = TRUE)
	{
		$stage = new User_stages($_SESSION['session_id'], $_SESSION['partner']);
		
		// Partner is ready when on the same or higher level
		if ( ($stage->module == $_SESSION['flow'] AND $stage->part >= $_SESSION['part']) OR  $stage->module > $_SESSION['flow'])
		{
			if ($xhttp)
			{
				echo 'yes';
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			if ($xhttp)
			{
				echo 'no';
			}
		}
		
		return FALSE;
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Callback for game results
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_result()
	{
		$game = lastchar($_SESSION['section']);
		
		$result = new Results($_SESSION['id'], $game, $_SESSION['part'], $_SESSION['id']);
		
		// Check if results are already stored
		if($result->id)
		{
			exit('Results already posted');
		}
		
		// Check post vars
		if( ! isset($_POST['resourcesToCPA']) || ! isset($_POST['resourcesToIPA']))
		{
			exit('Invalid results posted');
		}
		
		$result->user_id = $_SESSION['id'];
		$result->username = $_SESSION['username'];
		$result->partner_id = $_SESSION['partner'];
		$result->partnername = $_SESSION['partnername']; 
		$result->CPA = $_POST['resourcesToCPA'];
		$result->IPA = $_POST['resourcesToIPA'];
		$result->session_id = $_SESSION['session_id'];
		$result->game = $game;
		$result->round = $_SESSION['part'];
		$result->create();
		
		// Check if there's a timeout
		if( isset($_POST['timeout']) && $_POST['timeout'])
		{
			// Create partner results (partner might be unavailable, we have to move on)
			$result->id = 0;
			$result->user_id = $_SESSION['partner'];
			$result->username = $_SESSION['partnername'];
			$result->partner_id = $_SESSION['id'];
			$result->partnername = $_SESSION['username'];
			$result->CPA = 0;
			$result->IPA = 0;
			$result->create();
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Calculate profits after both players have submitted their investments
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_calculate()
	{
		if ($_POST)
		{
			$game = lastchar($_SESSION['section']);
			
			// Get game settings
			$settings = game_settings($_SESSION['session_id']);
			
			// Get game config for this session and game
			$game_conf = new Game_config($_SESSION['session_id'], $game);
			
			// Get my results
			$my_results = new Results($_SESSION['session_id'], $game, $_SESSION['part'], $_SESSION['id']);
			
			// get partner results
			$partner_results = new Results($_SESSION['session_id'], $game, $_SESSION['part'], $_SESSION['partner']);
						
			// Calculate and save my profit
			$my_results->calc_profit_round($partner_results, $settings, $game_conf)->update();
		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * Get current time on the server, used for countdown
	 *
	 * @param void
	 * @return void
	 * @author bochoven
	 **/
	function xhttp_servertime()
	{
		$now = new DateTime();
		echo $now->format("M j, Y H:i:s O")."\n";
	}
	
	
}