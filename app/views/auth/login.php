<?php $this->view('partials/head')?>
	
	
	<?php require($GLOBALS['lang_path']) ?>


   <body onload="document.user_login_form.username.focus();">
   
			<div id='form_centered'>
			<form name="user_login_form" method="post" action="">
			
					<p><label for='username'><?php echo($lang['str_USERNAME']); ?>: </label> 
							<input type="text" name="username" value="<?=$username?>"></p>
							
					<p><label for='password'><?php echo($lang['str_PASSWORD']); ?>: </label>
							<input type="password" name="password"></p>
							
					<p><label for='buttons'></label>      
							<input type="submit" value='<?php echo($lang['button_OK']); ?>'>&nbsp;&nbsp;
							<input type="reset" value='<?php echo($lang['button_CANCEL']); ?>'></p>
							
			</form>
			</div>
   </body>

</html>
