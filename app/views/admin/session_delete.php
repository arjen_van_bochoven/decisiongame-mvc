<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/modal.css" type="text/css">
	</head>
	<body>
		
		<h1>Verwijder sessie</h1>
		
		<?php if($removed):?>
		
		<p><b>Sessie verwijderd.</b><br>Sluit dit venster en ga terug naar het dashboard</p>
		
		<?php else:?>
		
		
		
		<form method='post' action='<?=url("admin/session_delete/$session_id")?>'>
			<p>Hiermee wordt alle data van de sessie verwijderd uit de database</p>
			<input type="hidden" name="confirm" value="true" />
			<input type="submit" value="Verwijder sessie">
		</form>
		
		<?php endif?>
	</body>
</html>



