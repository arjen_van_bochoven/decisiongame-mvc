<?php $this->view('partials/head')?>

<?php require($GLOBALS['lang_path']) ?>


	<body>    
	

		<?php $this->view('partials/admin_nav', array('lang' => $lang))?>


		<h2>Snel bekijken</h2>
		
		<p>
			<a class="modal" href="<?=url("admin/results/$session_id")?>">Resultaten bekijken</a><br>
			<a class="modal" href="<?=url("admin/userlist/$session_id")?>">Spelerslijst bekijken</a><br>
			<a class="modal" href="<?=url("admin/session_conf/$session_id")?>">Sessie configuratie bekijken</a><br>
		</p>
		
		<h2>Downloads</h2>
		
		<p>
			<a href="<?=url("admin/excel_dump/settings/$session_id")?>">Configuratie downloaden</a><br>
			<a href="<?=url("admin/excel_dump/chat/$session_id")?>">Chat downloaden</a><br>
			<a href="<?=url("admin/excel_dump/calculator_usage/$session_id")?>">Calculator gebruik downloaden</a><br>
			<a href="<?=url("admin/excel_dump/game_results/$session_id")?>">Game results downloaden</a><br>
			<a href="<?=url("admin/excel_dump/profits_to_pay/$session_id")?>">Winst resultaten downloaden</a><br>
			<a href="<?=url("admin/excel_dump/partnerships/$session_id")?>">Partnerlijst downloaden</a><br>
		</p>
		
		<h2>Wijzigen sessie</h2>
			
		<p>
			<a class="modal" href="<?=url("admin/session_edit/$session_id")?>">Sessie wijzigen</a><br>
			<a class="modal" href="<?=url("admin/session_reset/$session_id")?>">Sessie resetten</a><br>
			<a class="modal" href="<?=url("admin/session_delete/$session_id")?>">Sessie verwijderen</a><br>
		</p>
		
		<h2>Debug sessie</h2>
	
		<p>
			<a href="<?=url("admin/debug_login_users/$session_id")?>">Alle spelers inloggen</a><br>
		</p>
		
		
		<script src="<?=WEB_FOLDER?>assets/js/simple_modal.js"></script>
		
		<script type="text/javascript" charset="utf-8">
		
			// Attach modal to all items with class='modal'
			set_modals();
						
		</script>

	</body>
</html>



