<?php $this->view('partials/head')?>

<?php require($GLOBALS['lang_path']) ?>

	<BODY>
		
		<div class='admin-nav'>
			<span class="date"><?=$lang['str_TODAY']?> <?=$date_today?></span>
			<a href="<?=url('auth/logout/admin')?>">admin <?=$lang['button_LOGOUT']?></a>
		</div>

	<!-- new-session-button -->
		<p><a class="modal" href="<?=url('admin/session_edit')?>"><?=$lang['button_NEW_SESSION']?></a></p>

   <!-- sessions -->
		<TABLE id='sessions'>
			<TR>
				<TH><?=$lang['str_SESSION_NAME']?></TH>
				<TH><?=$lang['str_SESSION_DATE']?></TH>
				<TH><?=$lang['str_SESSION_COMMENT']?></TH>
				<TH><?=$lang['str_ALREADY_PLAYED']?></TH>
				<TH></TH>
			</TR>
	<?php foreach($sessions as $session):?>
			<?php $sid = $session->session_id?>
			<TR>
				<TD>
					<a href="<?=url("admin/session/$session->session_id")?>"><?=$session->session_name?></a>
				</TD>
				<TD><?=$session->datetime?></TD>
				<TD><?=$session->comment?></TD>
				<TD><?=$session->played?></TD>
				<TD></TD>
			</TR>
	<?php endforeach?>
		</TABLE>

		<script src="<?=WEB_FOLDER?>assets/js/simple_modal.js"></script>
		
		<script type="text/javascript" charset="utf-8">
			set_modals();
		</script>
		
	</BODY>
</HTML>
