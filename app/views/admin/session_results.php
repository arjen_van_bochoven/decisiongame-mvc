<?php header('Content-Type: text/html; charset=utf-8')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/modal.css" type="text/css">
	</head>
	<body>
		<?php
			// Get total profits per user
			$result = new Results();
			$profits_to_pay = $result->get_profits_session($session_id);
			
			// Get EE factor
			$settings = game_settings($session_id);
			$EE_factor = $settings->EE_factor;
		?>		
		
		<h1>Resultaten</h1>
		
		<p><a href="<?=url("admin/excel_dump/profits_to_pay/$session_id")?>">Download resultaten</a></p>
		
		<!-- profits to pay in real Euros -->
			<table>
				<tr><th>Gebruikersnaam</th><th>Winst in EE</th><th>Winst</th></tr>
		<?php foreach($profits_to_pay AS $user):?>
				<tr>
					<td><?=$user->username?></td>
					<td><?=$user->profit_ThisSession?></td>
					<td><?=sprintf('€ %1.2f', $user->profit_ThisSession * $EE_factor )?></td>
				</tr>
		<?php endforeach?>
			</table>
			
	</body>
</html>




