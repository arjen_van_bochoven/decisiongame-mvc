<?php $this->view('partials/head')?>

<?php require($GLOBALS['lang_path']) ?>



	<BODY>    
	
		<?php $this->view('partials/admin_nav', array('lang' => $lang))?>
		
		<?php
		
		
		$dbh = getdbh();
		$url = url('admin/user_progress/');
				
		$sql = "SELECT p.id as module, name, type
		from program_flow p
		ORDER BY p.id";
		
		$stmt = $dbh->prepare( $sql );
		$stmt->execute(array($session_id));
		$total = 0;
		?>
		
		<table class="admintable">
			<thead>
				<tr>
					<th>Stage</th>
					<th>Aantal</th>
					<th>Spelers</th>
					<th>Actie</th>
				</tr>
			</thead>
		<?php while ( $stage = $stmt->fetch( PDO::FETCH_OBJ ) ):?>
			<tr>
				<td><?=$stage->name?></td>
				<td>0</td>
				<td></td>
				<td>
					<?php if($stage->type=='wait'):?>
					<form class="game_start" method="post" action="<?=url('admin/xhttp_start_game/'.$session_id)?>">
						<input name="module" type="hidden" value="<?=$stage->module?>">
						<button>Start game</button>
					</form>
					<?php endif?>
				</td>
			</tr>
		<?php endwhile?>
			<tr class="total" style="border-top: 2px solid #000; font-weight: bold">
				<td>Totaal</td>
				<td>0</td>
				<td></td><td></td>
			</tr>
		</table>
		
		<script src="<?=WEB_FOLDER?>assets/js/simple_modal.js"></script>
			
		</script>
		
		<script type="text/javascript" charset="utf-8">
		
			// Userlist
			var userlist = [];
			// Get modules
			var modlist = $('tbody tr');
			
			// Get userlist
			$.getJSON('<?=url("admin/xhttp_user_list/$session_id")?>', function(data) {
				userlist = data;
				ajax_progress();
			});
			
			
			
			function ajax_progress()
			{
				// Initialize lists
				tbllist = [];
				cntlist = [];
				total = 0;
				for (var i = 0; i < modlist.length; i++) 
				{
					tbllist[i] = '';
					cntlist[i] = 0;
				}

				// Get user stages
				$.getJSON('<?=url("admin/xhttp_user_stages/$session_id")?>', function(data) {
					
					// Put users in table
					$.each(data, function(key, val) {
						key = '<a class="modal" href="<?=url("admin/user_progress/")?>' + userlist[key] + '">' + userlist[key] + '</a>'
						tbllist[val] == undefined ? tbllist[val] = key : tbllist[val] += ' ' + key
						cntlist[val]++;
						total++;
					});
					
					// Update number of players + playerlist
					$.each(modlist, function(key, val)
					{
						$(val).children(':nth-child(2)').html(cntlist[key+1]);
						$(val).children(':nth-child(3)').html(tbllist[key+1]);
					});
					
					// Set total players
					$('tr.total td:nth-child(2)').html(total);
					
					
					
					// Attach simple modal
					$('a.modal').click(function (e) {
						e.preventDefault();
						
						var src = $(this).attr('href');
						
						$.modal('<iframe src="' + src + '" height="320" width="410" style="border:0">', {
							containerCss:{
								backgroundColor:"#fff",
								borderColor:"#fff",
								height:350,
								padding:0,
								width:430
							},
							overlayClose:true
						});
					});
					
					setTimeout("ajax_progress()", 2000); //todo: get from settings
				});
			}
			
			// Game start buttons
			$('form.game_start').submit(function(e) {
				
				e.preventDefault();
				url = $(this).attr('action');
				
				$.post( url, $(this).serialize(),
					function( err ) {
						if( err )
						{
							alert(err);
						}
						else
						{
							alert('Spel gestart')
						}
				});
				
			  return false;
			});
			
		</script>
				
	</BODY>
</HTML>



