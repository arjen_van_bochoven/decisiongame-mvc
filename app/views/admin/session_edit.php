<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/modal.css" type="text/css">
	</head>
	<body>
		
		<h1><?=$session->id ? "Wijzig sessie" : "Nieuwe sessie"?></h1>
		
		<form method="post" action="<?=url("admin/session_save/$session->id")?>">
		<table>
			<tr>
				<th>Naam</th>
				<td><input style="font-size: 1.2em" name="session_name" value="<?=$session->session_name?>"></td>
			</tr>
			<tr>
				<th>Aanmaak datum</th>
				<td><?=$session->datetime?></td>
			</tr>
			<tr>
				<th>Commentaar</th>
				<td><textarea rows="2" cols="40" name="comment"><?=$session->comment?></textarea></td>
			</tr>
			<tr>
				<th>Aantal spelers</th>
				<td><input name="number_of_players_per_session" value="<?=$settings->number_of_players_per_session?>"></td>
			</tr>
			<tr>
				<th>Aantal spellen</th>
				<td><?=$settings->number_of_games?></td>
			</tr>
			<tr>
				<th>Thresholds</th>
				<td>
					Threshold 1<input name="default_threshold1" value="<?=$settings->default_threshold1?>"><br>
					Threshold 2<input name="default_threshold2" value="<?=$settings->default_threshold2?>"><br>
					Threshold 3<input name="default_threshold3" value="<?=$settings->default_threshold3?>">
				</td>
			</tr>
			<tr>
				<th>Rondes per spel</th>
				<td><input name="default_number_of_rounds" value="<?=$settings->default_number_of_rounds?>"></td>
			</tr>
			
			<tr>
				<th>Spelduur</th>
				<td>
					<input name="clock_minutes" value="<?=$settings->clock_minutes?>">Minuten<br>
					<input name="clock_seconds" value="<?=$settings->clock_seconds?>">Seconden
				</td>
			</tr>
			<tr>
				<th>Resources</th>
				<td>
					<input name="resources" value="<?=$settings->resources?>"><br>
				</td>
			</tr>
			<tr>
				<th>Fixed bonus</th>
				<td>
					<input name="fixed_bonus" value="<?=$settings->fixed_bonus?>"><br>
				</td>
			</tr>
			<tr>
				<th>EE factor</th>
				<td>
					<input name="EE_factor" value="<?=$settings->EE_factor?>"><br>
				</td>
			</tr>
			<tr>
				<th>Resource alloc</th>
				<td>
					Resource 1<input name="resource_allocation_1" value="<?=$settings->resource_allocation_1?>"><br>
					Resource 2<input name="resource_allocation_2" value="<?=$settings->resource_allocation_2?>"><br>
					Resource 3<input name="resource_allocation_3" value="<?=$settings->resource_allocation_3?>">
				</td>
			</tr>
			
		</table>
			<p>
				<input type="submit" value="<?=$session->id ? "Bewaar wijzigingen" : "Maak aan"?>">
				<a href="javascript:parent.location.reload();">Annuleer</a>
			</p>
		</form>
		
		<script src="<?=WEB_FOLDER?>assets/js/jquery.js"></script>

		<script type="text/javascript" charset="utf-8">
			// Game start buttons
			$('form').submit(function(e) {
				
				e.preventDefault();
				url = $(this).attr('action');
				
				$.post( url, $(this).serialize(),
					function( err ) {
						if(err)
						{
							alert(err)
						}
						parent.location.reload();
				});
				
			  return false;
			});
			
		</script>
	
	</body>
</html>



