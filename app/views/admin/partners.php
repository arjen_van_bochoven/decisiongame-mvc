<?php $this->view('partials/head')?>

<?php require($GLOBALS['lang_path']) ?>



	<BODY>    
	
		<?php $this->view('partials/admin_nav', array('lang' => $lang))?>
		
		<?php
		
		$game_rounds = array();
		$dbh = getdbh();
		$url = url('admin/user_partners/');
		
		$sql = "SELECT DISTINCT gamenumber, roundnumber FROM partnerships p WHERE session_id = ?";
		
		$stmt = $dbh->prepare( $sql );
		$stmt->execute(array($session_id));
		while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) )
		{
			$game_rounds[] = $rs;
		}
		?>
		
		<?php if(!$game_rounds):?>
		
		<p class="error">Geen partners gevonden in database, leg eerst de partners vast</p>
		
		<?php else:?>
		
		<p><a href="<?=url("admin/excel_dump/partnerships/$session_id")?>">Partnerlijst downloaden</a></p>
		<?php endif?>
		
		<?php foreach($game_rounds as $game_round):?>
		
		<h2>Game <?=$game_round->gamenumber?> Round <?=$game_round->roundnumber?></h2>
		
		<?php
		
		// Increase group_concat max
		$sql = "SET SESSION group_concat_max_len = 1000000";
		$stmt = $dbh->prepare( $sql );
		$stmt->execute();
		
		$sql = "SELECT history, partnerchange_game1 as pcg1, partnerchange_game$game_round->gamenumber as partnerchange,
		group_concat(concat('<b><a href=\"$url', user_id1, '\">', username1, '</a>&nbsp;<a href=\"$url', user_id2, '\">', username2, '</a></b>' ) ORDER BY username1 SEPARATOR ' ') AS partners 
		FROM partnerships p
		JOIN users u on (p.user_id1 = u.id)
		where p.session_id = ? AND gamenumber = ? AND roundnumber = ? AND user_id1 < user_id2 
		group by history, partnerchange_game1, partnerchange_game$game_round->gamenumber
		ORDER BY history, partnerchange_game$game_round->gamenumber";
		$stmt = $dbh->prepare( $sql );
		
		$stmt->execute(array($session_id, $game_round->gamenumber, $game_round->roundnumber));
		
		?>
		<table id="partner_table" class="admintable">
			<thead>
				<tr>
					<th>Group</th>
					<th>Partners</th>
				</tr>
			</thead>
		<?php while ( $partner = $stmt->fetch( PDO::FETCH_OBJ ) ):?>
			<tr>
				<td>
					<?=($partner->history ? 'History' : 'No&nbsp;history')?> <br>
					<?php if($game_round->gamenumber > 1):?>
					Game 1 <?=($partner->pcg1 ? 'Partnerchange' : 'No&nbsp;partnerchange') ?><br>
					<?php endif?>
					<?=($partner->partnerchange ? 'Partnerchange' : 'No&nbsp;partnerchange') ?>
				</td>
				<td><?=$partner->partners?></td>
			</tr>
		<?php endwhile?>
		</table>
		
		<?php endforeach?>
		
		<script src="<?=WEB_FOLDER?>assets/js/simple_modal.js"></script>
		
		<script type="text/javascript" charset="utf-8">
			
			$('b>a').addClass('modal');
			
			// Attach modal to all items with class='modal'
			set_modals();
						
		</script>
		
		
	</BODY>
</HTML>



