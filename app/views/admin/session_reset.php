<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/modal.css" type="text/css">
	</head>
	<body>
		
		<h1>Reset sessie</h1>
		
		<?php if($reset):?>
		
		<p><b>Sessie gereset.</b></p>
		
		<?php else:?>
		
		
		
		<form method='post' action='<?=url("admin/session_reset/$session_id")?>'>
			<p>Hiermee wordt de volgende data van de sessie verwijderd uit de database:</p>
			<ul>
				<li>Resultaten</li>
				<li>Partnerships</li>
				<li>Spel voortgang</li>
				<li>Calculator gebruik</li>
				<li>Chat</li>
			</ul>
			<input type="hidden" name="confirm" value="true" />
			<input type="submit" value="Reset sessie">
		</form>
		
		<?php endif?>
	</body>
</html>



