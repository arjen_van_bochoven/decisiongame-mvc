<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<style type="text/css" media="screen">
			body {background:#fff; color:#333; font: 16px/22px verdana, arial, sans-serif; height:100%; margin:0 auto; width:100%;}
			h1 {color:#3a6d8c; font-size:34px; line-height:40px; margin:0;}
			table {width: 400px; margin-top: 20px}
			th { text-align: right; background: #ddd}
		</style>
	</head>
	<body>
		<?php
		
		
		$dbh = getdbh();
		$url = url('admin/user_progress/');
		
		// Get user info
		$sql = "SELECT u.id, u.history, pf.name, u.session_id, u.password, 
			us.module, us.part, us.datetime, pf.type
		FROM users u 
		LEFT JOIN user_stages us USING (username)
		LEFT JOIN program_flow pf ON (pf.id = module)
		WHERE u.username = ?";
		
		$stmt = $dbh->prepare( $sql );
		$stmt->execute(array($username));
		if($user = $stmt->fetch( PDO::FETCH_OBJ ))
		{
			$session_id = $user->session_id;
		}
		else
		{
			die("User: $username not found");
		}
		
		// Check if we're in a game
		$game = 0;
		if($user->type == 'game')
		{
			$game = lastchar($user->name);
			
			// Get partner
			$partner = new Partnerships($user->id, $user->session_id, $game, $user->part);
		}
		
		// Get Program flow info
		$flow = new Programflow();
		$stages = $flow->retrieve_many('id > 1 ORDER BY sortorder');
		
		?>		
		
		<h1>Speler <?=$username?></h1>
		
		<table class="playertable" style="border: 1px solid #ddd">
			<tr>
				<th>Wachtwoord</th><td><?=$user->password?></td>
			</tr>
			<tr>
				<th>History</th><td><?=$user->history?'Ja':'Nee'?></td>
			</tr>
			<tr>
				<th>Module</th><td><?=$user->name?></td>
			</tr>
			<tr>
				<th>Ronde</th><td><?=$game?$user->part:'n.v.t.'?></td>
			</tr>
			<tr>
				<th>Partner</th>
				<td><?php if($game):?>
					<a href="<?=$url.$partner->username2?>"><?=$partner->username2?></a>
				<?php else:?>
				n.v.t
				<?php endif?>
				</td>
			</tr>
			<tr>
				<th>Laatste update</th><td><?=$user->datetime?></td>
			</tr>
		</table>
		
		
		<form action="<?=$url.$username?>" method="post" accept-charset="utf-8">
		<table class="playertable" style="border: 1px solid #ddd">
			<caption>Pas speler module aan</caption>
			<tr>
				<td>Module: </td>
				<td>
					<select name="flow" id="flow" style="width: 140px">
					<?php foreach($stages as $stage):?>
						<option value="<?=$stage->id?>" <?=$stage->id == $user->module ? 'selected="selected"' : ''?>>
						<?=$stage->name?>
						</option>
					<?php endforeach?>
					</select>
				</td>
				<td><input type="submit" value="Pas aan"></td>
			</tr>
		</table>
		</form>
		
	</body>
</html>



