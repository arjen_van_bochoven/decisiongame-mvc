<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/modal.css" type="text/css">
	</head>
	<body>
		
		<h1>Configuratie</h1>
		
		<a href="<?=url("admin/excel_dump/settings/$session_id")?>">Configuratie downloaden</a>
							
		<table id='session_characteristics'>
		<tr>
			<th>Sessie naam:</th>
			<td><?=$session_name?></td>
		</tr>
		<tr>
			<th>Aanmaakdatum:</th>
			<td><?=$session_obj->datetime?></td>
		</tr>
		<tr>
			<th>Beschrijving:</th>
			<td><?=$session_obj->comment?></td>
		</tr>
		<tr>
			<th>Aantal beschikbare gebruikers:</th>
			<td><?=$available_users?></td>
		</tr>
	   	<tr>
			<th>Aantal spellen:</th>
			<td><?=$settings->number_of_games?></td>
		</tr>
		<?php for ($game=1; $game <= $settings->number_of_games; $game++):?>
		<tr>
			<th>Drempelwaarde spel <?=$game?>:</th>
			<td><?=$settings->{'default_threshold'.$game} ? $settings->{'default_threshold'.$game} : 0?></td>
		</tr>
		<?php endfor?>
		<tr>
			<th>Aantal rondes per spel:</th>
			<td>7</td>
		</tr>
	   	<tr>
			<th>Speeltijd per ronde:</th>
			<td><?=$settings->clock_minutes?> minuten <br>
			<?=$settings->clock_seconds?> seconden</td>
		</tr>
	   	<tr>
			<th>Resources:</th>
			<td><?=$settings->resources?></td>
		</tr>
		<tr>
			<th>Resource 1:</th>
			<td><?=$settings->resource_allocation_1?></td>
		</tr>
	   	<tr>
			<th>Resource 2:</th>
			<td><?=$settings->resource_allocation_2?></td>
		</tr>
	   	<tr>
			<th>Resource 3:</th>
			<td><?=$settings->resource_allocation_3?></td>
		</tr>
		<tr>
			<th>Vaste bonus:</th>
			<td><?=$settings->fixed_bonus?></td>
		</tr>
		<tr>
			<th>Omreken factor:</th>
			<td><?=$settings->EE_factor?></td>
		</tr>
		
		</table>
	</body>
</html>



