<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/modal.css" type="text/css">
	</head>
	<body>
		<?php
			$dbh = getdbh();
			
			
		
			// Get partnerships info
			$sql = "SELECT *
			FROM partnerships
			WHERE user_id1 = ?
			ORDER BY gamenumber, roundnumber";
		
			$stmt = $dbh->prepare( $sql );
			$stmt->execute(array($user_id));
			
			$user = new Users($user_id);
		?>		
		
		<h1>Partners voor <?=$user->username?></h1>
		
		
		<table>
			<tr><th>Game</th><th>Round</th><th>Partner</th></tr>
			<?php while($user = $stmt->fetch( PDO::FETCH_OBJ )):?>
			<tr>
				<td><?=$user->gamenumber?></td>
				<td><?=$user->roundnumber?></td>
				<td><a href="<?=url("admin/user_partners/$user->user_id2")?>"><?=$user->username2?></a></td>
			</tr>
			<?php endwhile?>
		</table>
	</body>
</html>



