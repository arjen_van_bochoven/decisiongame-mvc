<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/modal.css" type="text/css">
	</head>
	<body>
		<?php
			$dbh = getdbh();
		
			// Get user info
			$sql = "SELECT *
			FROM users
			WHERE session_id = ?
			ORDER BY id";
		
			$stmt = $dbh->prepare( $sql );
			$stmt->execute(array($session_id));
		?>		
		
		<h1>Spelerlijst</h1>
		
		<p><a href="<?=url("admin/excel_dump/users/$session_id")?>">Download spelerlijst</a></p>
		
		<table>
			<tr><th>Username</th><th>Password</th></tr>
			<?php while($user = $stmt->fetch( PDO::FETCH_OBJ )):?>
			<tr>
				<td><?=$user->username?></td>
				<td><?=$user->password?></td>
			</tr>
			<?php endwhile?>
		</table>
	</body>
</html>



