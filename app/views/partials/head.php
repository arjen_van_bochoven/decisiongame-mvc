<?php header('Content-Type: text/html; charset=ISO-8859-1')?>
<!DOCTYPE html>
<html>

	<head>
		<title>Beslissingsspel</title>
		<!-- icon in adres-bar -->
		<link rel="shortcut icon" href="<?=WEB_FOLDER?>assets/euro.ico" type="image/ico">
		<!-- stylesheet -->
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/style.css" type="text/css">
	<?php if(isset($textpage)):?>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/user_style_text.css" type="text/css">
	<?php elseif(isset($admin)):?>
		<LINK REL="stylesheet" HREF="<?=WEB_FOLDER?>assets/css/admin_style.css" TYPE="text/css">
	<?php else:?>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/user_style.css" type="text/css">
	<?php endif?>
	
	<?php if(defined('DEBUG')):?>
		<link rel="stylesheet" href="<?=WEB_FOLDER?>assets/css/debug.css" type="text/css">
	<?php endif?>

		<script type="text/javascript" charset="utf-8" src="<?=WEB_FOLDER?>assets/js/jquery.js"></script>
	<?php if(isset($admin)):?>
		<script type="text/javascript" charset="utf-8" src="<?=WEB_FOLDER?>assets/js/decisiongame_admin.js"></script>
	<?php else:?>
		<script type="text/javascript" charset="utf-8" src="<?=WEB_FOLDER?>assets/js/decisiongame.js"></script>
	<?php endif?>
		<script type="text/javascript" charset="utf-8">
			var baseurl="<?=url('')?>";
		</script>
		</head>

<?php if(defined('DEBUG') AND ! isset($admin)):?>
	<?php $this->view('partials/debug_menu')?>
<?php endif?>