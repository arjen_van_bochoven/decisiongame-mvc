<?php
	$flow = new Programflow();
	$stages = $flow->retrieve_many('id > 1 ORDER BY sortorder');
?>

<div class="debug_session">
<?php if($_SESSION):?>
	<b>Session variables</b><br />
<?php foreach($_SESSION as $skey => $svar):?>
	<?=$skey?>: <?=$svar?><br />
<?php endforeach?>
	<b>Jump to:</b>
	<form action="<?=url('user/advance')?>" method="post">
		<select name="flow" id="flow" style="width: 140px">
		<?php foreach($stages as $stage):?>
			<option value="<?=$stage->id?>" <?=$stage->id == $_SESSION['flow'] ? 'selected="selected"' : ''?>>
			<?=$stage->name?>
			</option>
		<?php endforeach?>
		</select>
	</form>
<?php else:?>
Debug menu active
<?php endif?>
</div>
