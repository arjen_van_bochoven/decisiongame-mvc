<H4><?php echo ($lang['str_DECISION_HISTORY']);?></H4>
<TABLE id='decision_history'>
	<TR>
		<TD><?=$lang['str_ROUND_NUMBER']?></TD>
		<TD><?=$lang['str_GAME_PARTNER']?></TD>
		<TD><?=$lang['str_CONTROLLED_RESOUCES']?></TD>
		<TD><?=$lang['str_INVESTED_RESOURCES_CPA']?></TD>
		<TD><?=$lang['str_INVESTED_RESOURCES_IPA']?></TD>
		<TD><?=$lang['str_THRESHOLD']?></TD>
		<TD><?=$lang['str_IPA_IN_EURO']?></TD>
		<TD><?=$lang['str_ROUND_PROFIT']?></TD>
		<TD><?=$lang['str_GAME_PROFIT']?></TD>
	</TR>
	<?php foreach ($history_list AS $history):?>
	<TR>
		<TD><?=$history->round?></TD>
		<TD><?=$history->partnername?></TD>
		<TD><?=$history->resources?></TD>
		<TD><?=$history->CPA?></TD>
		<TD><?=$history->IPA?></TD>
		<TD><?=$history->threshold_reached ? $lang['str_THRESHOLD_REACHED_yes'] : $lang['str_THRESHOLD_REACHED_no']?></TD>
		<TD><?=$history->total_profit_IPAinEuro?></TD>
		<TD><?=$history->total_profit_ThisRound?></TD>
		<TD><?=$history->total_profit_ThisGame?></TD>
	</TR>
	<?php endforeach?>
</TABLE>