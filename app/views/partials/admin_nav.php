<div class='admin-nav'>
	<span class="date"><?=$lang['str_TODAY']?> <?=$date_today?> | <?=$session_name?></span>
	<a href="<?=url('admin/overview')?>">Dashboard</a> |
	<a href="<?=url("admin/session/$session_id")?>">Sessie control</a> |
	<a href="<?=url("admin/session_progress/$session_id")?>">Sessie voortgang</a> |
	<a href="<?=url("admin/partners/$session_id")?>">Partnerlijst</a> |
	<a href="<?=url("auth/logout/admin")?>">admin <?=$lang['button_LOGOUT']?></a>
</div>