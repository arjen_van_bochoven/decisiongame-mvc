<?php 
	$sql="	SELECT count(u.`id`) as number, GROUP_CONCAT(username SEPARATOR ', ') AS users,
				history, partnerchange_game1, partnerchange_game2, partnerchange_game3
			FROM users u
			JOIN partnerships p on (p.`user_id1` = u.`id`)
			WHERE p.`session_id` = ? AND gamenumber = 1 and roundnumber = 1
			GROUP BY history, partnerchange_game1, partnerchange_game2, partnerchange_game3;";
		$dbh = getdbh();
		$stmt = $dbh->prepare( $sql );
		$stmt->execute(array($session_id));
		
			$groups = array(0 => 'B1.2', 1 => 'A1.2', 2 => 'B2.1', 3 => 'A2.1', 4 => 'B1.3', 5 => 'A1.3', 6 => 'B2.2', 7 => 'A2.2');
		
			?>
		<table style="width: 100%; margin: 0; padding: 20px">
			<caption>Groepen</caption>
			<tr>
				<th>Groep</th>
				<th>Aantal</th>
				<th>Spelers</th>
			</tr>
		<?php while ( $rs = $stmt->fetch( PDO::FETCH_OBJ ) ):?>
			<tr>
				<td><?=$groups[$rs->history + $rs->partnerchange_game1 * 2 + $rs->partnerchange_game2 * 4]?> (<?=$rs->history ? 'History' : 'No history'?>, <?=$rs->partnerchange_game1 ? 'diff' : 'same'?> partner, <?=$rs->partnerchange_game2 ? 'diff' : 'same'?> partner, <?=$rs->partnerchange_game3 ? 'diff' : 'same'?> partner)</td>
				<td><?=$rs->number?></td>
				<td><?=$rs->users?></td>
			</tr>
		<?php endwhile?>
		</table>