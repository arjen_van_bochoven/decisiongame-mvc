<?php 
/* ------------------------------------------------------------
name:				calculator.php
date:				19 september 2011
author:			Rita Standhaft
purpose:			calculator; 
-------------------------------------------------------------*/        
?>
<FORM NAME="calculator" METHOD="POST" ACTION="user_calculator_save_usage.php">

	<TABLE id='calculator'>
		<TR>
			<TD COLSPAN=3><INPUT NAME="Display" SIZE="20" disabled></TD>
			<TD><INPUT TYPE="reset" VALUE=" C " disabled></TD>
		</TR>	

		<TR>
			<TD><INPUT TYPE="button" VALUE=" 7 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" 8 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" 9 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" + " disabled></TD>
		</TR>

		<TR>
			<TD><INPUT TYPE="button" VALUE=" 4 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" 5 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" 6 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" - " disabled></TD>
		</TR>

		<TR>
			<TD><INPUT TYPE="button" VALUE=" 1 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" 2 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" 3 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" * " disabled></TD>
		</TR>

		<TR>
			<TD><INPUT TYPE="button" VALUE=" = " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" 0 " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" . " disabled></TD>
			<TD><INPUT TYPE="button" VALUE=" / " disabled></TD>
		</TR>
	</TABLE>

</FORM>