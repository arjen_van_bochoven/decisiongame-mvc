<?php $this->view('partials/head')?>

	<?php require($GLOBALS['lang_path']) ?>
	
	<?php $settings = game_settings($_SESSION['session_id'])?>


	<BODY>
		
		<FORM id="GoToNextRound_form" METHOD="POST" ACTION='<?=url("user/advance/$flow_next/$part_next")?>'>
			<input type="hidden" name="flow" value='<?php echo $_SESSION['flow'] ?>'>
			<input type='hidden' name='part' value='<?php echo $part_next?>' />
		</FORM>
		

	<!-- round-characteristics and username   -->
	<DIV id='above'>
		 <P id='logout'><?php echo ($username); ?></p>

		<UL id='round_characteristics'>
			<LI><LABEL FOR='gamenumber'><?php echo ($lang['str_GAME']); ?>:</LABEL><?php echo ($gamenumber);?></LI>
			<LI><LABEL FOR='waitforpartner'><?php echo ($lang['str_WAIT_FOR_PARTNER']); ?>:</LABEL><?php echo ($roundnumber);?></LI>
		</UL>
	</DIV>


	<!-- vertical line -->
		<HR class="line_above">
		

	<!-- game-decision -->
		<H4><?php echo ($lang['str_DECISION']);?></H4>
	
	   <P id='decision_text'><?php echo ($lang['str_DECISION_TEXT']);?></P>

<!--		<DIV id='game_decision_endofgame'> -->
		<P id='game_decision_endofgame'><?php echo ($lang['str_WAITING_PARTNER_INFO']);?>
		<BR><BR><BR>
		<IMG SRC="<?=WEB_FOLDER?>assets/gfx/waiting.gif" WIDTH="32" HEIGHT="32" />
		<BR><BR><BR>
<!--	   </DIV> -->

		
	<!-- calculator -->
	   <P id='calculator_area'>
			<?php $this->view('partials/calculator_disabled')?> 
		</P> 
		

	<!-- vertical line -->
		<HR>
		
		
	<!-- history -->
		<?php $this->view('partials/history', array('lang' => $lang))?>


	<!-- vertical line -->
		<HR>
		
		
	<!-- chatbox -->
	<H4><?php echo ($lang['str_CHATBOX']);?></H4>
		<DIV id='chatbox_area'>
				<DIV id='chatbox_content'></DIV>
		</DIV>


<!-- vertical line -->
		<HR>


	<!-- game-characteristics -->
		<H4><?php echo ($lang['str_CHARACTERISTICS']);?></H4>
		<TABLE id='game_characteristics'>
		<TR><TD WIDTH="300"><?php echo ($lang['str_RESOURCES']); ?>:</TD><TD ALIGN=RIGHT WIDTH="80"><?php echo ($settings->resources);?></TD><TD></TD></TR>
		<TR><TD COLSPAN=3 HEIGHT='15'></TD></TR>
		<TR><TD><?php echo ($lang['str_RESOURCE_ALLOCATION']); ?>:</TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_2);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION2']); ?></TD></TR>
		<TR><TD></TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_3);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION3']); ?></TD></TR>
		<TR><TD></TD><TD COLSPAN=2><I><?php echo ($lang['str_RESOURCE_EXPLANATION4']);?></I></TD></TR>
	   	<TR><TD COLSPAN=3 HEIGHT='15'></TD></TR>
	   	<TR><TD><?php echo ($lang['str_FIXED_BONUS']); ?>:</TD><TD ALIGN=RIGHT><?php echo ($settings->fixed_bonus);?></TD><TD></TD></TR>
		<TR><TD></TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_1);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION1']); ?></TD></TR>
		<TR><TD COLSPAN=3 ALIGN=RIGHT><I><?php echo ($lang['str_RESOURCE_EXPLANATION5']);?></I></TD></TR>
		</TABLE>		


	<!-- footer -->
	   <DIV id='footer'>&nbsp;</DIV>
	
		<script type="text/javascript" charset="utf-8">
			var partner_frequency = <?php echo $settings->partner_frequency?> // Poll freq		
			var waitforpartner_callback = '<?=url("user/xhttp_wait_for_partner")?>'; // Wait for partner callback url
		</script>


	</BODY>
</HTML>