<?php $this->view('partials/head', array('textpage' => TRUE))?>

<?php require($GLOBALS['lang_path']) ?>


	<BODY>

	<DIV id='above'>
					<P id='logout'><?php echo ($username); ?></p>
	</DIV>


	<!-- vertical line -->
		<HR class='line_above'>


	<!--text -->
		<DIV id='intro'>
			<?php echo ($text); ?>
		</DIV>


	<!-- vertical line -->
		<HR>

		
	<!-- forward-button -->
		<FORM METHOD="post" ACTION='<?php echo ($link . '&token=' . $username); ?>'> 
			<P id='forward' ALIGN=RIGHT>
				<?php if(defined('DEBUG')) echo '<span class="debug_link">DEBUG: '. $link . '&token=' . $username. '</span>'?>
				<INPUT TYPE="submit" VALUE='<?php echo($lang['button_FORWARD']); ?>'>
			</p>
		</FORM>


	<?php if (defined('DEBUG')):?>
		
		<BR>
		<FORM NAME="GoNext_form" METHOD="POST" ACTION='<?=url("user/advance/$flow_next/$part_next")?>'>
			<input type='hidden' name='flow' value='<?php echo $flow_next?>' />
			<input type='hidden' name='part' value='<?=$part_next?>' />
			<P id='forward' ALIGN=RIGHT><INPUT TYPE="submit" VALUE='Shortcut: Ga verder zonder vragenlijst'></p>
		</FORM>

	<?php endif?>
	<!-- footer -->
	   <DIV id='footer'>&nbsp;</DIV>


	</BODY>
</HTML>