<?php $this->view('partials/head')?>

	<?php require($GLOBALS['lang_path']) ?>
	
	<?php $settings = game_settings($_SESSION['session_id'])?>


	<BODY>

	<!-- round-characteristics and username   -->
	<DIV id='above'>
			 <P id='logout'><?php echo ($username); ?></p>

		<UL id='round_characteristics'>
			<LI><LABEL FOR='gamenumber'><?php echo ($lang['str_GAME']); ?>:</LABEL><?php echo ($gamenumber);?></LI>
			<LI><LABEL FOR='endofgame'><?php echo ($lang['str_END_OF_GAME']); ?></LABEL></LI>
		</UL>
		

	</DIV>


	<!-- vertical line -->
		<HR class="line_above">
		

	<!-- game-decision -->
		<H4><?php echo ($lang['str_DECISION']);?></H4>
	
	   <P id='decision_text'><?php echo ($lang['str_DECISION_TEXT']);?></P>

		<DIV id='game_decision_endofgame'> 
			<P class='endofgame_message'><?php echo ($lang['str_END_OF_GAME_GO_FORWARD']);?></P>			

				<!-- forward-button -->
					<FORM METHOD="post" ACTION='<?=url("user/advance/$flow_next/$part_next")?>'><BR><BR><BR>
						<P id='forward' ALIGN=RIGHT><INPUT TYPE="submit" VALUE='<?php echo($lang['button_FORWARD']); ?>'></P>
						<input type='hidden' name='flow' value='<?=$flow_next?>' />
						<input type='hidden' name='part' value='<?=$part_next?>' />
					</FORM>
	   </DIV> 

		
	<!-- calculator -->
	   <P id='calculator_area'>
			<?php $this->view('partials/calculator_disabled')?> 
		</P> 
		

	<!-- vertical line -->
		<HR>
		
		
	<!-- history -->
		<?php $this->view('partials/history', array('lang' => $lang))?>


	<!-- vertical line -->
		<HR>
		
		
	<!-- chatbox -->
	<H4><?php echo ($lang['str_CHATBOX']);?></H4>
		<DIV id='chatbox_area'>
				<DIV id='chatbox_content'></DIV>
		</DIV>


<!-- vertical line -->
		<HR>


	<!-- game-characteristics -->
		<H4><?php echo ($lang['str_CHARACTERISTICS']);?></H4>
		<TABLE id='game_characteristics'>
		<TR><TD WIDTH="300"><?php echo ($lang['str_RESOURCES']); ?>:</TD><TD ALIGN=RIGHT WIDTH="80"><?php echo ($settings->resources);?></TD><TD></TD></TR>
		<TR><TD COLSPAN=3 HEIGHT='15'></TD></TR>
		<TR><TD><?php echo ($lang['str_RESOURCE_ALLOCATION']); ?>:</TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_2);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION2']); ?></TD></TR>
		<TR><TD></TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_3);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION3']); ?></TD></TR>
		<TR><TD></TD><TD COLSPAN=2><I><?php echo ($lang['str_RESOURCE_EXPLANATION4']);?></I></TD></TR>
	   	<TR><TD COLSPAN=3 HEIGHT='15'></TD></TR>
	   	<TR><TD><?php echo ($lang['str_FIXED_BONUS']); ?>:</TD><TD ALIGN=RIGHT><?php echo ($settings->fixed_bonus);?></TD><TD></TD></TR>
		<TR><TD></TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_1);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION1']); ?></TD></TR>
		<TR><TD COLSPAN=3 ALIGN=RIGHT><I><?php echo ($lang['str_RESOURCE_EXPLANATION5']);?></I></TD></TR>
		</TABLE>		


	<!-- footer -->
	   <DIV id='footer'>&nbsp;</DIV>


	</BODY>
</HTML>



