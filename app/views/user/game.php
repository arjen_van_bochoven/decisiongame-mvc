<?php $this->view('partials/head')?>

	<?php require($GLOBALS['lang_path']) ?>
	
	<?php $settings = game_settings($_SESSION['session_id'])?>


	<BODY> 

		<FORM id="GoToNextRound_form" METHOD="POST" ACTION='<?=url("user/advance/$flow_next/$part_next")?>'>
			<input type='hidden' name='flow' value='<?php echo $flow_next?>' />
			<input type='hidden' name='part' value='<?php echo $part_next?>' />
		</FORM>


	<!-- round-characteristics and username	  -->
	<DIV id='above'>
			 <P id='logout'><?php echo ($username); ?></p>

		<UL id='round_characteristics'>
			<LI><LABEL FOR='gamenumber'><?php echo ($lang['str_GAME']); ?>:</LABEL><?php echo ($gamenumber);?></LI>
			<LI><LABEL FOR='roundnumber'><?php echo ($lang['str_ROUND']); ?>:</LABEL><?php echo ($roundnumber);?></LI>
		</UL>

	</DIV>
	
	
	<!-- vertical line -->
		<HR class="line_above">


	<!-- game-decision -->
		<H4><?php echo ($lang['str_DECISION']);?></H4>
		
	   <P id='decision_text'><?php echo ($lang['str_DECISION_TEXT']);?></P>

	   <FORM id="decision_form" NAME="decision_form" METHOD=POST ACTION="<?=url("user/xhttp_result")?>" autocomplete="off">	 

		<DIV id='game_decision'>
			<!-- clock -->
			<P><I><SPAN id='time_rests_message'><?php echo ($lang['str_REMAINING_TIME']);?></SPAN>:</I>&nbsp;&nbsp;<SPAN ID="theTime" CLASS="timeClass"></SPAN></P>
	
	   <UL id='decision_area'>
			<LI><INPUT TYPE="text" NAME="resourcesToCPA" SIZE="10">&nbsp;&nbsp;<?php echo ($lang['str_RESOURCES_TO_CPA']); ?></LI>
			<LI><INPUT TYPE="text" NAME="resourcesToIPA" SIZE="10">&nbsp;&nbsp;<?php echo ($lang['str_RESOURCES_TO_IPA']); ?></LI>
			<LI><INPUT TYPE="submit" VALUE='<?php echo ($lang['button_ENTER_DECISION']); ?>' </LI>
	   </UL>
	   
			<P id='register_message'><?php echo ($lang['str_DECLARATION_REGISTERED']); ?></P>

	   </DIV>
		<input type="hidden" name="timeout" value="0" />
	   </FORM>	

		
	<!-- calculator -->
	   <P id='calculator_area'>
			<?php $this->view('partials/calculator')?> 
		</P> 
		

	<!-- vertical line -->
		<HR class="line_middle">


	<!-- history -->
	<?php $this->view('partials/history', array('lang' => $lang))?>


	<!-- vertical line -->
		<HR class="line_middle">


	<!-- chatbox -->
		<H4><?php echo ($lang['str_CHATBOX']);?></H4>
		<DIV id='chatbox_area'>
		<FORM id='chat_form' METHOD="POST" ACTION="<?=url("user/xhttp_send_chat")?>" autocomplete="off">
		<INPUT TYPE="HIDDEN" NAME="chatline2">
				<DIV id='chatbox_content'></DIV>
		
					<P id='chatline'><INPUT TYPE="text" NAME="chatline" SIZE="80" maxlength="255" />
					<INPUT TYPE='submit' VALUE='<?php echo ($lang['button_SEND_CHAT']); ?>'>
				</P>
		</FORM>
		</DIV>


	<!-- vertical line -->
		<HR class="line_middle">


	<!-- game-characteristics -->
		<H4><?php echo ($lang['str_CHARACTERISTICS']);?></H4>
		<TABLE id='game_characteristics'>
		<TR><TD WIDTH="300"><?php echo ($lang['str_RESOURCES']); ?>:</TD><TD ALIGN=RIGHT WIDTH="80"><?php echo ($settings->resources);?></TD><TD></TD></TR>
		<TR><TD COLSPAN=3 HEIGHT='15'></TD></TR>
		<TR><TD><?php echo ($lang['str_RESOURCE_ALLOCATION']); ?>:</TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_2);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION2']); ?></TD></TR>
		<TR><TD></TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_3);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION3']); ?></TD></TR>
		<TR><TD></TD><TD COLSPAN=2><I><?php echo ($lang['str_RESOURCE_EXPLANATION4']);?></I></TD></TR>
		<TR><TD COLSPAN=3 HEIGHT='15'></TD></TR>
		<TR><TD ROWSPAN=2><?php echo ($lang['str_FIXED_BONUS']); ?>:</TD><TD ALIGN=RIGHT><?php echo ($settings->fixed_bonus);?></TD><TD></TD></TR>
		<TR><TD COLSPAN=2><I><?php echo ($lang['str_RESOURCE_EXPLANATION7']);?></I></TD></TR>
		<TR><TD COLSPAN=3 ALIGN=RIGHT><I><?php echo ($lang['str_RESOURCE_EXPLANATION6']);?></I></TD></TR>
		<TR><TD COLSPAN=3 HEIGHT='15'></TD></TR>
		<TR><TD></TD><TD ALIGN=RIGHT>EE <?php echo ($settings->resource_allocation_1);?></TD><TD><?php echo ($lang['str_RESOURCE_EXPLANATION1']); ?></TD></TR>
		<TR><TD></TD><TD COLSPAN=2><I><?php echo ($lang['str_RESOURCE_EXPLANATION8']);?></I></TD></TR>
		<TR><TD COLSPAN=3 HEIGHT='10'></TD></TR>
		<TR><TD COLSPAN=3 ALIGN=RIGHT><I><?php echo ($lang['str_RESOURCE_EXPLANATION5']);?></I></TD></TR>
		</TABLE>


	<!-- footer -->
	   <DIV id='footer'>&nbsp;</DIV>
			
		<!-- countdown -->
		<SCRIPT language="JavaScript" src="<?=WEB_FOLDER?>assets/js/jquery.countdown.min.js"></SCRIPT>
	
		<!-- countdown-stopwatch -->
		<SCRIPT language="javascript" type="text/javascript">
			// All script globals are defined here
			var game_end_time = '<?=$game_end_time?>'; // End time for current game fixme
			var partner_frequency = <?php echo $settings->partner_frequency?> // Poll freq
			var chatbox_frequency = <?php echo $settings->chatbox_frequency?> // Chat poll freq
			var chat_callback = '<?=url("user/xhttp_chat")?>'; // Chat callback url
			var game_callback = '<?=url("user/xhttp_game")?>'; // Game callback url
			var servertime_callback = '<?=url("user/xhttp_servertime")?>'; // Servertime callback url
			var xhttp_calculate_url = '<?=url("user/xhttp_calculate")?>'; // Calculate Profits callback url
			var resources = <?=$settings->resources?>
			
			var error_negative = '<?=$lang['str_CHECK_NEGATIVE']?>'
			var error_decimal = '<?=$lang['str_CHECK_DECIMAL']?>'
			var error_values = '<?=$lang['str_CHECK_CPA_PLUS_IPA']?>'
			var remaining_time = '<?=$lang['str_REMAINING_TIME_2']?>'
			var last_chat_entry = 0; // Datetime of the last chat entry
			
			<?php if($results_posted):?> 
			var results_posted = 1;
			<?php endif?>
		</SCRIPT>

	</BODY>
</HTML>
